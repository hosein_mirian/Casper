package mirian.english.casper.Activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import mirian.english.casper.Helper.FlowLayout;
import mirian.english.casper.Helper.SoundManager;
import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.lists.Content;

public class ActivityType4 extends ActivityCheck implements TextToSpeech.OnInitListener {
  ArrayList<TextView> Texts = new ArrayList<>();
  Button check_bt;
  ImageView pic;
  EditText input_txt;
  TextView question_txt, title_txt;
  ArrayList<String> SpeechText = new ArrayList<>();
  ArrayList<String> Selected_text = new ArrayList<>();
  String answer = "";
  int page_number, stage_id;
  FlowLayout answer_grp, option_grp;
  ProgressBar progressBar;
  Dialog result_dialog;
  public View.OnClickListener result_click = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
      result_dialog.dismiss();

      if (answer.contentEquals(AppController.ContentList.get(page_number).getAnswer())) {
        //Toaster.toastGreen("درست گفتی !", 0,true);
//--------// set the status true because user answered right
        Content new_content = new Content(
          AppController.ContentList.get(page_number).getId(),
          AppController.ContentList.get(page_number).getStageId(),
          AppController.ContentList.get(page_number).getQuestion_type(),
          AppController.ContentList.get(page_number).getQuestion(),
          AppController.ContentList.get(page_number).getAnswer(),
          AppController.ContentList.get(page_number).getOption(),
          true // set the status true because user answered right
        );
        AppController.ContentList.set(page_number, new_content);
//-----------------------------------------------
        if ((page_number + 1) < AppController.ContentList.size()) {
          finish();
          ActivitySelector.Select(AppController.ContentList.get(page_number + 1).getQuestion_type(), page_number, stage_id);
        } else {
          //this is the last page... go to Activity Result or check if there is...
          finish();
          GotoActivity.gotoActivity(ActivityResult.class, AppController.context, -1, -1, null, null, null);

        }
      } else {
        //Toaster.toastRed("اشتباهه !!", 0,true);
        if ((page_number + 1) < AppController.ContentList.size()) {
          finish();
          ActivitySelector.Select(AppController.ContentList.get(page_number + 1).getQuestion_type(), page_number, stage_id);
        } else {
          //this is the last page... go to Activity Result or check if there is
          finish();
          GotoActivity.gotoActivity(ActivityResult.class, AppController.context, -1, -1, null, null, null);

        }

      }
      for (int i = 0; i < AppController.ContentList.size(); i++) {
        AppController.logger("status " + i + " is " + AppController.ContentList.get(i).getStatus());
      }

    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_type4);
    //-------------------Read Bundle----------------------------Define which category is selected...
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      page_number = extras.getInt("id");
      stage_id = extras.getInt("stage_id");
    }

    progressBar = (ProgressBar) findViewById(R.id.progress);
    progressBar.setMax(AppController.ContentList.size());
    progressBar.setProgress(page_number + 1);

    check_bt = (Button) findViewById(R.id.check_bt);
    check_bt.setTypeface(AppController.font.get(0));
    //------Read data and set question.
    question_txt = (TextView) findViewById(R.id.question_txt);
    question_txt.setTypeface(AppController.font.get(0));
    question_txt.setText(AppController.ContentList.get(page_number).getQuestion());
    //----- Text to Speach
    if (AppController.tts == null)
      AppController.tts = new TextToSpeech(AppController.context, this);
    //----Read Answer
    check_bt = (Button) findViewById(R.id.check_bt);
    check_bt.setTypeface(AppController.font.get(0));
    question_txt = (TextView) findViewById(R.id.question_txt);
    title_txt = (TextView) findViewById(R.id.title_txt);
    question_txt.setTypeface(AppController.font.get(5));
    title_txt.setTypeface(AppController.font.get(0));
    String speakout = getResources().getString(R.string.speakout);
    question_txt.setText(AppController.ContentList.get(page_number).getQuestion() + " " + speakout);
    question_txt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        AppController.tts.speak(question_txt.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
      }
    });
    pic = (ImageView) findViewById(R.id.ax_img);
//    int resourceID = AppController.context.getResources().getIdentifier(AppController.ContentList.get(page_number).getOption(), "mipmap", AppController.context.getPackageName());
//    pic.setImageResource(resourceID);
    try
    {
      // get input stream
      InputStream ims = AppController.context.getAssets().open(AppController.ContentList.get(page_number).getOption()+".jpg");
      // load image as Drawable
      Drawable d = Drawable.createFromStream(ims, null);
      // set image to ImageView
      pic.setImageDrawable(d);
      ims .close();
    }
    catch(IOException ex)
    {
      return;
    }

    input_txt = (EditText) findViewById(R.id.input_txt);
    input_txt.setTypeface(AppController.font.get(1));
    input_txt.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
      }

      @Override
      public void afterTextChanged(Editable editable) {
        if (!editable.toString().contentEquals("")) {
          check_bt.setEnabled(true);
        } else {
          check_bt.setEnabled(false);
        }
      }
    });
    check_bt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        answer = input_txt.getText().toString();
        //dialog show
        result_dialog = new Dialog(AppController.CurrentActivity);
        result_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        result_dialog.setContentView(R.layout.dialoganswer);
        result_dialog.setCancelable(false);
        result_dialog.show();
        TextView answer_txt = (TextView) result_dialog.findViewById(R.id.answer_txt);
        answer_txt.setTypeface(AppController.font.get(0));
        Button result_ok = (Button) result_dialog.findViewById(R.id.ok_bt);
        answer_txt.setText(AppController.ContentList.get(page_number).getAnswer());
        result_ok.setOnClickListener(result_click);
        result_ok.setTypeface(AppController.font.get(1));

        if (answer.contentEquals(AppController.ContentList.get(page_number).getAnswer())) {
          answer_txt.setTextColor(Color.GREEN);
          SoundManager.playBeep(1);
        }else
        {
          result_ok.setBackgroundColor(Color.RED);
          answer_txt.setTextColor(Color.RED);
          SoundManager.playBeep(2);
        }


      }
    });

  }

  @Override
  public void onInit(int i) {

  }
}
