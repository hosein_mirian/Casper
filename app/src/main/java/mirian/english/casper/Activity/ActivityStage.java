package mirian.english.casper.Activity;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInBottomAnimationAdapter;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.lists.Stage;
import mirian.english.casper.lists.StageAdapter;
import mirian.english.casper.webservice.LoadFromDatabase;
import mirian.english.casper.lists.Struct_Stage;

public class ActivityStage extends ActivityCheck {
  String title, pic, color;
  int cat_id; // what category is this...
  TextView cat_name_txt,help_sc;
  ImageView pic_img;
  ViewGroup activity_stage;
  RecyclerView recyclerView;
  StageAdapter adapter;
  List<Stage> StageList;
  Stage stage;
  int TABLE_SIZE = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_stage);
    //-------------------Read Bundle----------------------------Define which category is selected...
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      title = extras.getString("title");
      pic = extras.getString("big_pic");
      color = extras.getString("back_color");
      cat_id = extras.getInt("id");
    }
    help_sc = (TextView) findViewById(R.id.help_sc);
    help_sc.setTypeface(AppController.font.get(2));
    cat_name_txt = (TextView) findViewById(R.id.cat_name_txt);
    cat_name_txt.setText(title);
    cat_name_txt.setTypeface(AppController.font.get(0));
    activity_stage = (ViewGroup) findViewById(R.id.activity_stage);
    activity_stage.setBackgroundColor(Color.parseColor(color));
    pic_img = (ImageView) findViewById(R.id.pic);
    int resourceID = AppController.context.getResources().getIdentifier(pic, "mipmap", AppController.context.getPackageName());
    pic_img.setImageResource(resourceID);
    //-------------- ask a query to see how many courses we have for this category...
    recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);
    recyclerView.setLayoutManager(mLayoutManager);
    recyclerView.setHorizontalFadingEdgeEnabled(true);
    prepareAlbums();
  }

  //---------------------------------------prepare Album Function
  private void prepareAlbums() {
//---------Load info

    StageList = new ArrayList<>();
    adapter = new StageAdapter(this, StageList);

    final AnimationAdapter animationadapter = new SlideInBottomAnimationAdapter(adapter);
    animationadapter.setFirstOnly(true);
    animationadapter.setDuration(500);
    animationadapter.setInterpolator(new OvershootInterpolator(.5f));
    recyclerView.setAdapter(animationadapter);

    try {
      TABLE_SIZE = LoadFromDatabase.get_size("stages", cat_id);
      AppController.logger("Size of All Records : " + TABLE_SIZE); // Get the size of  table
      recyclerView.addItemDecoration(new ActivityStage.GridSpacingItemDecoration(TABLE_SIZE, dpToPx(-4), true));
//---------------------------------------INITIAL LOAD 10 Item...
      ArrayList<Struct_Stage> data = LoadFromDatabase.load("INITIAL_LOAD", "stages", cat_id); // return max id in the table as Arraylist<Struct>
      AppController.logger("size data: " + data.size()); // Max Id in the table is ...;
      for (int i = 0; i < data.size(); i++) {
        stage = new Stage(data.get(i).id, data.get(i).cat_id, data.get(i).stage_id, data.get(i).stage_name, data.get(i).desc);
        StageList.add(stage);
      }
      //close database
      AppController.database.close();
      //Update Adapter...
      adapter.notifyDataSetChanged();

    } catch (Exception e) {
      e.printStackTrace();
    }

  }
//----------------------------------End of Prepare()............

  /**
   * RecyclerView item decoration - give equal margin around grid item
   */
  public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int spacing;
    private boolean includeEdge;

    GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
      this.spanCount = spanCount;
      this.spacing = spacing;
      this.includeEdge = includeEdge;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
      int position = parent.getChildAdapterPosition(view); // item position
      int column = position % spanCount; // item column

      if (includeEdge) {
        outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
        outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

        if (position < spanCount) { // top edge
          outRect.top = spacing;
        }
        outRect.bottom = spacing; // item bottom
      } else {
        outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
        outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
        if (position >= spanCount) {
          outRect.top = spacing; // item top
        }
      }
    }
  }

  /**
   * Converting dp to pixel
   */
  private int dpToPx(int dp) {
    Resources r = getResources();
    return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
  }

}
