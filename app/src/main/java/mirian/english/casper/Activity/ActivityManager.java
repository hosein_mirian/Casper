package mirian.english.casper.Activity;

import android.app.Activity;

import java.util.ArrayList;

import mirian.english.casper.app.AppController;


public class ActivityManager {

    public static ArrayList<Activity> act2 = new ArrayList<Activity>();


    public static void ActivitySaver(Activity activity)
    {
        if ( !act2.contains(activity))
            act2.add(activity);
        //act[activityNumber] = activity;
    }

    public static void FinishSplashActivity(String ActivityName) { // close the Current ActivityName...
        if (AppController.CurrentActivity.getLocalClassName().equalsIgnoreCase(ActivityName)) {
            GotoActivity.gotoActivity(ActivityMain.class, AppController.CurrentActivity, -1,-1, null,null,null);
           // AppController.CurrentActivity.finish();
        }
    }
    public static void finishAll()
    {

        for (int i = 0; i < act2.size(); i++)
            act2.get(i).finish();

    }

}