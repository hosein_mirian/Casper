package mirian.english.casper.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.Markets_Helper.MarketIsInstalled;
import mirian.english.casper.Markets_Helper.MarketOpenPage;
import mirian.english.casper.Markets_Helper.MarketsName;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;


public class ActivityProduct extends ActivityCheck {
  private LinearLayout    product[] = new LinearLayout[17];
  private TextView    texts[] = new TextView[17];
  private View.OnClickListener myproduct = new View.OnClickListener() {

    @Override
    public void onClick(View arg0) {


      arg0.setBackgroundColor(Color.parseColor("#ffaf32"));
      switch (arg0.getId())
      {
        case R.id.bt0:
          MarketOpenPage.openLinkApp("hosein.learnenglishmovie");
          break;
        case R.id.bt1:
          MarketOpenPage.openLinkApp("imaj.hosein.sahamplus");
          break;
        case R.id.bt2:
          MarketOpenPage.openLinkApp("mirian.piano.test");
          break;
        case R.id.bt3:
          MarketOpenPage.openLinkApp("mirian.hoseinguitary");
          break;
        case R.id.bt4:
          MarketOpenPage.openLinkApp("mirian.guitar.test");
          break;
        case R.id.bt5:
          MarketOpenPage.openLinkApp("mirian.learnenglish2.fin");
          break;
        case R.id.bt6:
          MarketOpenPage.openLinkApp("mirian.bourse.technical");
          break;
        case R.id.bt7:
          MarketOpenPage.openLinkApp("mirian.diabet.test");
          break;
        case R.id.bt8:
          MarketOpenPage.openLinkApp("mirian.giah.test");
          break;
        case R.id.bt9:
          MarketOpenPage.openLinkApp("mirian.miveh.test");
          break;
        case R.id.bt10:
          MarketOpenPage.openLinkApp("darman.saratan.test");
          break;
        case R.id.bt11:
          MarketOpenPage.openLinkApp("mirian.ravan.test");
          break;
        case R.id.bt12:
          MarketOpenPage.openLinkApp("mirian.zanan.test");
          break;
        case R.id.bt13:
          MarketOpenPage.openLinkApp("mirian.zibae.test");
          break;
        case R.id.bt14:
          MarketOpenPage.openLinkApp("mirian.kodak.test");
          break;
        case R.id.bt15:
          MarketOpenPage.openLinkApp("mirian.etiad.test");
          break;
        case R.id.bt16:
          MarketOpenPage.openLinkApp("flashkartest.test");
          break;

      }

    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_product);
    product[0] = (LinearLayout) findViewById(R.id.bt0);
    product[1] = (LinearLayout) findViewById(R.id.bt1);
    product[2] = (LinearLayout) findViewById(R.id.bt2);
    product[3] = (LinearLayout) findViewById(R.id.bt3);
    product[4] = (LinearLayout) findViewById(R.id.bt4);
    product[5] = (LinearLayout) findViewById(R.id.bt5);
    product[6] = (LinearLayout) findViewById(R.id.bt6);
    product[7] = (LinearLayout) findViewById(R.id.bt7);
    product[8] = (LinearLayout) findViewById(R.id.bt8);
    product[9] = (LinearLayout) findViewById(R.id.bt9);
    product[10] = (LinearLayout) findViewById(R.id.bt10);
    product[11] = (LinearLayout) findViewById(R.id.bt11);
    product[12] = (LinearLayout) findViewById(R.id.bt12);
    product[13] = (LinearLayout) findViewById(R.id.bt13);
    product[14] = (LinearLayout) findViewById(R.id.bt14);
    product[15] = (LinearLayout) findViewById(R.id.bt15);
    product[16] = (LinearLayout) findViewById(R.id.bt16);

    texts[0] = (TextView) findViewById(R.id.tb0);
    texts[1] = (TextView) findViewById(R.id.tb1);
    texts[2] = (TextView) findViewById(R.id.tb2);
    texts[3] = (TextView) findViewById(R.id.tb3);
    texts[4] = (TextView) findViewById(R.id.tb4);
    texts[5] = (TextView) findViewById(R.id.tb5);
    texts[6] = (TextView) findViewById(R.id.tb6);
    texts[7] = (TextView) findViewById(R.id.tb7);
    texts[8] = (TextView) findViewById(R.id.tb8);
    texts[9] = (TextView) findViewById(R.id.tb9);
    texts[10] = (TextView) findViewById(R.id.tb10);
    texts[11] = (TextView) findViewById(R.id.tb11);
    texts[12] = (TextView) findViewById(R.id.tb12);
    texts[13] = (TextView) findViewById(R.id.tb13);
    texts[14] = (TextView) findViewById(R.id.tb14);
    texts[15] = (TextView) findViewById(R.id.tb15);
    texts[16] = (TextView) findViewById(R.id.tb16);

    for (int i = 0; i < product.length; i++)
    {
      texts[i].setTypeface(AppController.font.get(0));
      texts[i].setTextColor(Color.parseColor("#4d4d4d"));
      product[i].setOnClickListener(myproduct);
    }
  }
}
