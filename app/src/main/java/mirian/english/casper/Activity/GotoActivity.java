package mirian.english.casper.Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.util.List;

import mirian.english.casper.lists.Content;


public class GotoActivity {



    public static void gotoActivity(Class<?> classname, Context mycontext, int position,int stage_id, String title,String big_pic,String back_color)
    {
      Intent intent;

        intent = new Intent(mycontext, classname);
      if(title!=null)
        intent.putExtra("title", title);
      if(big_pic!=null)
        intent.putExtra("big_pic", big_pic);
      if(position != -1)
        intent.putExtra("id", position);
      if(back_color != null)
        intent.putExtra("back_color", back_color);
      if(stage_id != -1)
        intent.putExtra("stage_id", stage_id);

        intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        mycontext.startActivity(intent);
        // Appcontroller.CurrentActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

    }

  public static void gotoActivityMedia(Class<?> classname, Context mycontext,String media_type, String media_source , String media_subtitle)
  {
    Intent intent;

    intent = new Intent(mycontext, classname);
    if(media_type!=null)
      intent.putExtra("media_type", media_type);
    if(media_source!=null)
      intent.putExtra("media_source", media_source);
    if(media_subtitle!=null)
      intent.putExtra("media_subtitle", media_subtitle);

    intent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

    mycontext.startActivity(intent);

  }

    public static void gotoBazarActivity(String bazarpage, Context mycontext)
    {
        Uri uri = Uri.parse(bazarpage);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        try {

            mycontext.startActivity(intent);

        }
        catch (Exception e)
        {

            Log.i("negano", e.getMessage());

        }

    }

}
