package mirian.english.casper.Activity;

import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInBottomAnimationAdapter;
import mirian.english.casper.Helper.IsOnline;
import mirian.english.casper.Helper.RecommendApp;
import mirian.english.casper.Markets_Helper.MarketsComment;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.lists.Category;
import mirian.english.casper.lists.CategoryAdapter;
import mirian.english.casper.webservice.LoadFromDatabase;
import mirian.english.casper.lists.Struct_Category;


public class ActivityMain extends ActivityCheck implements NavigationView.OnNavigationItemSelectedListener {

  RecyclerView recyclerView;
  private CategoryAdapter adapter;
  private List<Category> categoryList;
  public static ArrayList<Integer> EachCategorySize = new ArrayList<>();
  ArrayList<Integer> WhichCategoryHasNew = new ArrayList<>();
  Category category;
  int TABLE_SIZE = 0;
  DrawerLayout drawer;
  RecyclerView.LayoutManager mLayoutManager;
  RecyclerView.ItemDecoration decor;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    AppController.logger("ActivityMain");

    //get user number and its personal information... if its the third time that user come into app then ...
    int count_entry = AppController.preferences.getInt("number_user_entered", 0);
    if (IsOnline.isOnline())
      if (count_entry == 2 || count_entry == 5 || count_entry == 8 || count_entry == 10) {
        AppController.handler.post(new Runnable() {
          @Override
          public void run() {
            RecommendApp.show_dialog();
          }
        });
      }
//---------------------------------------------------------
    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(false);
    drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.setDrawerListener(toggle);
    toggle.syncState();
    toggle.setDrawerIndicatorEnabled(false);
    Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.slider_bt, getApplicationContext().getTheme());
    toggle.setHomeAsUpIndicator(drawable);
    toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (drawer.isDrawerVisible(GravityCompat.START)) {
          drawer.closeDrawer(GravityCompat.START);
        } else {
          drawer.openDrawer(GravityCompat.START);
        }
      }
    });


    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
    recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

//    LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
//    recyclerView.setLayoutManager(mLayoutManager);

    mLayoutManager = new GridLayoutManager(this, 1);
    recyclerView.setLayoutManager(mLayoutManager);


    prepareAlbums();
  }

  @Override
  public void onBackPressed() {
    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }

  //---------------------------------------prepare Album Function
  private void prepareAlbums() {
//---------Load info
//    categoryList.clear();
//    adapter.notifyDataSetChanged();

    //mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true);

    categoryList = new ArrayList<>();
    adapter = new CategoryAdapter(this, categoryList);


    final AnimationAdapter animationadapter = new SlideInBottomAnimationAdapter(adapter);
    animationadapter.setFirstOnly(true);
    animationadapter.setDuration(500);
    animationadapter.setInterpolator(new OvershootInterpolator(.5f));
    recyclerView.setAdapter(animationadapter);


    TABLE_SIZE = LoadFromDatabase.get_size("category", -1);
    AppController.logger("Size of All Records : " + TABLE_SIZE); // Get the size of  table


    recyclerView.removeItemDecoration(decor);
    decor = new GridSpacingItemDecoration(TABLE_SIZE, dpToPx(-4), true);
    recyclerView.addItemDecoration(decor);

//---------------------------------------INITIAL LOAD 10 Item...

    ArrayList<Struct_Category> data = LoadFromDatabase.load("INITIAL_LOAD", "category", -1); // return max id in the table as Arraylist<Struct>
    AppController.logger("size data: " + data.size()); // Max Id in the table is ...;
    for (int i = 0; i < data.size(); i++) {
      category = new Category(data.get(i).cat_id, data.get(i).cat_name, data.get(i).pic, data.get(i).color);
      categoryList.add(category);
    }
    //close database
    AppController.database.close();
    //Update Adapter...
    adapter.notifyDataSetChanged();


  }

  @SuppressWarnings("StatementWithEmptyBody")
  @Override
  public boolean onNavigationItemSelected(@NonNull MenuItem item) {

    // Handle navigation view item clicks here.
    int id = item.getItemId();

    if (id == R.id.nav_camera) {
      if (IsOnline.isOnline())
        MarketsComment.comment();
      // Handle the camera action
    } else if (id == R.id.nav_gallery) {
      GotoActivity.gotoActivity(ActivityProduct.class, AppController.context, -1, -1, null, null, null);
    }

    drawer.closeDrawer(GravityCompat.START);
    return true;

  }
//----------------------------------End of Prepare()............

  /**
   * RecyclerView item decoration - give equal margin around grid item
   */
  public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int spacing;
    private boolean includeEdge;

    GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
      this.spanCount = spanCount;
      this.spacing = spacing;
      this.includeEdge = includeEdge;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
      int position = parent.getChildAdapterPosition(view); // item position
      int column = position % spanCount; // item column

      if (includeEdge) {
        outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
        outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

        if (position < spanCount) { // top edge
          outRect.top = spacing;
        }
        outRect.bottom = spacing; // item bottom
      } else {
        outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
        outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
        if (position >= spanCount) {
          outRect.top = spacing; // item top
        }
      }
    }
  }

  /**
   * Converting dp to pixel
   */
  private int dpToPx(int dp) {
    Resources r = getResources();
    return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
  }

  @Override
  protected void onRestart() {
    super.onRestart();

    AppController.logger("restart");
    prepareAlbums();
  }


}
