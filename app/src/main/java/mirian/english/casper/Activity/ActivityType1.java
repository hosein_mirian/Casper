package mirian.english.casper.Activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.speech.tts.TextToSpeech;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import mirian.english.casper.Helper.SoundManager;
import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.lists.Content;

public class ActivityType1 extends ActivityCheck implements TextToSpeech.OnInitListener {

  ArrayList<ImageView> Images = new ArrayList<>();
  ArrayList<TextView> Texts = new ArrayList<>();
  Button check_bt;
  TextView question_txt;
  ArrayList<String> SpeechText = new ArrayList<>();
  private int answer;
  int page_number, stage_id;
  ProgressBar progressBar;
  Dialog result_dialog;
  public View.OnClickListener result_click = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
      //---------
      result_dialog.dismiss();

      if (answer == Integer.valueOf(AppController.ContentList.get(page_number).getAnswer())) {
        //Toaster.toastGreen("درست گفتی !", 0, true);
//--------// set the status true because user answered right
        Content new_content = new Content(
          AppController.ContentList.get(page_number).getId(),
          AppController.ContentList.get(page_number).getStageId(),
          AppController.ContentList.get(page_number).getQuestion_type(),
          AppController.ContentList.get(page_number).getQuestion(),
          AppController.ContentList.get(page_number).getAnswer(),
          AppController.ContentList.get(page_number).getOption(),
          true // set the status true because user answered right
        );
        AppController.ContentList.set(page_number, new_content);
//-----------------------------------------------

        //-------
        if ((page_number + 1) < AppController.ContentList.size()) {
          finish();
          ActivitySelector.Select(AppController.ContentList.get(page_number + 1).getQuestion_type(), page_number, stage_id);
        } else {
          //this is the last page... go to Activity Result or check if there is
          finish();
          GotoActivity.gotoActivity(ActivityResult.class, AppController.context, -1, -1, null, null, null);
        }
      } else {
        //Toaster.toastRed("اشتباهه !!", 0, true);
        if ((page_number + 1) < AppController.ContentList.size()) {
          finish();
          ActivitySelector.Select(AppController.ContentList.get(page_number + 1).getQuestion_type(), page_number, stage_id);
        } else {
          //this is the last page... go to Activity Result or check if there is
          finish();
          GotoActivity.gotoActivity(ActivityResult.class, AppController.context, -1, -1, null, null, null);
        }

      }
      for (int i = 0; i < AppController.ContentList.size(); i++) {
        AppController.logger("status " + i + " is " + AppController.ContentList.get(i).getStatus());
      }


    }
  };

  View.OnClickListener ImageClickListener = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
      check_bt.setEnabled(true);

      switch (view.getId()) {

        case R.id.img1:
          AppController.tts.speak(SpeechText.get(0), TextToSpeech.QUEUE_FLUSH, null);
          answer = 1;
          Images.get(0).setBackgroundColor(Color.GREEN);
          Images.get(1).setBackgroundColor(Color.TRANSPARENT);
          Images.get(2).setBackgroundColor(Color.TRANSPARENT);
          Images.get(3).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(0).setBackgroundColor(Color.GREEN);
          Texts.get(1).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(2).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(3).setBackgroundColor(Color.TRANSPARENT);
          break;
        case R.id.img2:
          AppController.tts.speak(SpeechText.get(1), TextToSpeech.QUEUE_FLUSH, null);
          answer = 2;
          Images.get(1).setBackgroundColor(Color.GREEN);
          Images.get(0).setBackgroundColor(Color.TRANSPARENT);
          Images.get(2).setBackgroundColor(Color.TRANSPARENT);
          Images.get(3).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(1).setBackgroundColor(Color.GREEN);
          Texts.get(0).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(2).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(3).setBackgroundColor(Color.TRANSPARENT);
          break;
        case R.id.img3:
          AppController.tts.speak(SpeechText.get(2), TextToSpeech.QUEUE_FLUSH, null);
          answer = 3;

          Images.get(2).setBackgroundColor(Color.GREEN);
          Images.get(0).setBackgroundColor(Color.TRANSPARENT);
          Images.get(1).setBackgroundColor(Color.TRANSPARENT);
          Images.get(3).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(2).setBackgroundColor(Color.GREEN);
          Texts.get(0).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(1).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(3).setBackgroundColor(Color.TRANSPARENT);
          break;
        case R.id.img4:
          AppController.tts.speak(SpeechText.get(3), TextToSpeech.QUEUE_FLUSH, null);
          answer = 4;

          Images.get(3).setBackgroundColor(Color.GREEN);
          Images.get(0).setBackgroundColor(Color.TRANSPARENT);
          Images.get(1).setBackgroundColor(Color.TRANSPARENT);
          Images.get(2).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(3).setBackgroundColor(Color.GREEN);
          Texts.get(0).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(1).setBackgroundColor(Color.TRANSPARENT);
          Texts.get(2).setBackgroundColor(Color.TRANSPARENT);
          break;
      }
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_type1);
    AppController.logger("ActivityType 1");
    //-------------------Read Bundle----------------------------Define which category is selected...
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      page_number = extras.getInt("id");
      stage_id = extras.getInt("stage_id");
    }

    progressBar = (ProgressBar) findViewById(R.id.progress);
    progressBar.setMax(AppController.ContentList.size());
    progressBar.setProgress(page_number + 1);

    check_bt = (Button) findViewById(R.id.check_bt);
    check_bt.setTypeface(AppController.font.get(0));
    Images.add((ImageView) findViewById(R.id.img1));
    Images.add((ImageView) findViewById(R.id.img2));
    Images.add((ImageView) findViewById(R.id.img3));
    Images.add((ImageView) findViewById(R.id.img4));
    Texts.add((TextView) findViewById(R.id.title1));
    Texts.add((TextView) findViewById(R.id.title2));
    Texts.add((TextView) findViewById(R.id.title3));
    Texts.add((TextView) findViewById(R.id.title4));
    //----Read data and set options
/*
    new Thread(new Runnable() {
      @Override
      public void run() {

      }
    }).start();
*/
    ArrayList<String> ax = new ArrayList<>();
    ax.add("man");
    ax.add("boy");
    ax.add("woman");
    ax.add("apple");
    String finalOutput;
    int resourceID;
    Scanner subLine = new Scanner(AppController.ContentList.get(page_number).getOption()).useDelimiter(",");
    int j = -1;
    while (subLine.hasNext()) {
      j++;
      finalOutput = subLine.next();
      //StringBuilder sb = new StringBuilder(finalOutput);
      AppController.logger("finalOutput " + finalOutput);
      SpeechText.add(finalOutput);
      Texts.get(j).setText(finalOutput);
      Texts.get(j).setTypeface(AppController.font.get(1));
      //Toaster.toast(finalOutput,0);
      Images.get(j).setOnClickListener(ImageClickListener);

      try
      {
        // get input stream
        InputStream ims = AppController.context.getAssets().open(finalOutput+".jpg");
        // load image as Drawable
        Drawable d = Drawable.createFromStream(ims, null);
        // set image to ImageView
        Images.get(j).setImageDrawable(d);
        ims .close();
      }
      catch(IOException ex)
      {
        return;
      }

//      AppController.logger("FF :" + SpeechText.get(j));
//      resourceID = AppController.context.getResources().getIdentifier(finalOutput, "mipmap", AppController.context.getPackageName());
//      //Glide.with(this).load(resourceID).into(Images.get(j));
//      Images.get(j).setImageResource(resourceID);

    }

/*
    String str = AppController.ContentList.get(page_number).getOption();
    String temp="";
    StringBuilder sa = new StringBuilder(str);
    for (int i = 0; i < sa.length(); i++) {
      if(!sa.substring(i,i+1).contentEquals(","))
      {
        AppController.logger("i"+i);
        temp += sa.subSequence(i,i+1);
      }else
      {
        AppController.logger("else");
        SpeechText.add(temp);
        AppController.logger("SpeechText " + SpeechText);
        temp ="";
      }
    }
    SpeechText.add(temp);
*/


    //------Read data and set question.
    question_txt = (TextView) findViewById(R.id.question_txt);
    question_txt.setTypeface(AppController.font.get(0));
    question_txt.setText(AppController.ContentList.get(page_number).getQuestion());
    //-------------

    //----- Text to Speach
    if (AppController.tts == null)
      AppController.tts = new TextToSpeech(AppController.context, this);

    //check if the user has chosen the right answer
    check_bt.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View view) {
        //dialog show
        result_dialog = new Dialog(AppController.CurrentActivity);
        result_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        result_dialog.setContentView(R.layout.dialoganswer);
        result_dialog.setCancelable(false);
        result_dialog.show();
        TextView answer_txt = (TextView) result_dialog.findViewById(R.id.answer_txt);
        answer_txt.setTypeface(AppController.font.get(0));
        Button result_ok = (Button) result_dialog.findViewById(R.id.ok_bt);
        answer_txt.setText(String.valueOf(SpeechText.get(Integer.parseInt(AppController.ContentList.get(page_number).getAnswer()) - 1)));
        result_ok.setOnClickListener(result_click);
        result_ok.setTypeface(AppController.font.get(1));
        if (answer == Integer.valueOf(AppController.ContentList.get(page_number).getAnswer())) {
          answer_txt.setTextColor(Color.GREEN);
          SoundManager.playBeep(1);
        } else {
          result_ok.setBackgroundColor(Color.RED);
          answer_txt.setTextColor(Color.RED);
          SoundManager.playBeep(2);
        }

      }
    });

  }

  @Override
  public void onInit(int status) {
    if (status == TextToSpeech.SUCCESS) {
      int result = AppController.tts.setLanguage(Locale.US);
      // tts.setPitch(5); // set pitch level
      // tts.setSpeechRate(2); // set speech speed rate
      if (result == TextToSpeech.LANG_MISSING_DATA
        || result == TextToSpeech.LANG_NOT_SUPPORTED) {
        Log.e("TTS", "Language is not supported");
      } else {
      }
    } else {
      Log.e("TTS", "Initilization Failed");
    }

  }


}
