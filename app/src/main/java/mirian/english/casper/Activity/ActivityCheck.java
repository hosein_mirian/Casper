package mirian.english.casper.Activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.Markets_Helper.MarketsComment;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;


public class ActivityCheck extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  protected void onResume() {
    super.onResume();
    AppController.CurrentActivity = this;
    ActivityManager.ActivitySaver(this);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
  }


  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (AppController.database != null)
      AppController.database.close();

    // Don't forget to shutdown!
    if (AppController.tts != null) {
      AppController.tts.stop();
      //AppController.tts.shutdown();
    }


  }


  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK) {

      if (AppController.CurrentActivity.getLocalClassName().equalsIgnoreCase("Activity.ActivityMain")) {
        final Dialog dialog = new Dialog(AppController.CurrentActivity);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogexit);
        TextView confirm_txt = (TextView) dialog.findViewById(R.id.confirm_txt);

        dialog.show();
        Button Yes_bt = (Button) dialog.findViewById(R.id.yes_bt);
        Button No_bt = (Button) dialog.findViewById(R.id.no_bt);
        confirm_txt.setTypeface(AppController.font.get(0));
        Yes_bt.setTypeface(AppController.font.get(0));
        No_bt.setTypeface(AppController.font.get(0));
        Yes_bt.setOnClickListener(new View.OnClickListener() {

          @Override
          public void onClick(View arg0) {
            dialog.dismiss();
            ActivityManager.finishAll();
            if (AppController.Check_User_Entry) {
              MarketsComment.comment();
              Toaster.toastGreen("لطفا با ستاره ها و نظرات خود از ما حمایت کنید.", 0,false);
            }
          }
        });
        No_bt.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View arg0) {
            dialog.dismiss();
          }
        });
      } else if(!AppController.CurrentActivity.getLocalClassName().equalsIgnoreCase("Activity.ActivityStage") && !AppController.CurrentActivity.getLocalClassName().equalsIgnoreCase("Activity.ActivityResult") && !AppController.CurrentActivity.getLocalClassName().equalsIgnoreCase("Activity.ActivityProduct") && !AppController.CurrentActivity.getLocalClassName().equalsIgnoreCase("Activity.ActivityPurchase")) {
        final Dialog dialog = new Dialog(AppController.CurrentActivity);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialogexit);
        TextView confirm_txt = (TextView) dialog.findViewById(R.id.confirm_txt);

        dialog.show();
        Button Yes_bt = (Button) dialog.findViewById(R.id.yes_bt);
        Button No_bt = (Button) dialog.findViewById(R.id.no_bt);
        confirm_txt.setText("آیا میخواهید از این درس خارج شوید !؟ ");
        confirm_txt.setTypeface(AppController.font.get(0));
        Yes_bt.setTypeface(AppController.font.get(0));
        No_bt.setTypeface(AppController.font.get(0));
        Yes_bt.setOnClickListener(new View.OnClickListener() {

          @Override
          public void onClick(View arg0) {
            dialog.dismiss();
            finish();
          }
        });
        No_bt.setOnClickListener(new View.OnClickListener() {

          @Override
          public void onClick(View arg0) {
            dialog.dismiss();
          }
        });

      }
      return super.onKeyDown(keyCode, event);
    }
    return true;
  }
}



