package mirian.english.casper.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.Markets_Helper.MarketIsInstalled;
import mirian.english.casper.Markets_Helper.MarketsName;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.util.IabHelper;
import mirian.english.casper.util.IabResult;
import mirian.english.casper.util.Inventory;
import mirian.english.casper.util.Purchase;


public class ActivityPurchase extends ActivityCheck {
  Button purchase_bt;
  TextView desc_txt;
  ProgressDialog dialog;
  // SKUs for our products: the premium upgrade (non-consumable)
  String SKU_PREMIUM = "casper"; // all markets SKU

  // Does the user have the premium upgrade?
  // (arbitrary) request code for the purchase flow
  static final int RC_REQUEST = 10001;
  // The helper object
  IabHelper mHelper;
  public static String KEY = "CASPER_PREMIUM";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_purchase);
    purchase_bt = (Button) findViewById(R.id.purchase_bt);
    desc_txt = (TextView) findViewById(R.id.desc);
    purchase_bt.setTypeface(AppController.font.get(0));
    desc_txt.setTypeface(AppController.font.get(1));
    purchase_bt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        onCustomPremiumAppButtonClicked();
      }
    });
    // load your setting that are you premium or not?
    AppController.IS_USER_PREMIUM = AppController.preferences.getBoolean(KEY, false);
    if (AppController.IS_USER_PREMIUM) {
      Toaster.toastGreen("شما کاربر ویژه هستید.", 0,false);
      finish();
      return;
    }
    // Check if market is installed on this device then do the others...
    Boolean Check_Market_Is_Installed = MarketIsInstalled.isPackageInstalled(MarketsName.getMarketPackage());
    if (!Check_Market_Is_Installed) {// if its not installed then ...
      Toaster.toastRed("اپلیکیشن "+MarketsName.getMarketName()+" را نصب کرده و مجددا تلاش نمایید.", 0,false);
      finish();
      return;
    }
    // this codes happen if user is not premium. and also if the market program is installed on device..--------------------------------------------
    dialog = new ProgressDialog(this);
    dialog.setMessage("در حال اتصال ...");
    dialog.setCancelable(false);
    dialog.show();
    // code that u get from bazar.
    String base64EncodedPublicKey ="MIHNMA0GCSqGSIb3DQEBAQUAA4G7ADCBtwKBrwDMdGQfXRHmF38srkOG2nrrA1G8/d1K9nHJ1a5jmiQTrfbGHaosk356GPc4297QIerKtgrYEOvkh4vcvkDS1qe/b28I12EA69Rqv78oMK/0wNdZg37RKiLV24/vCJY8Ep8xfg29j+RUzNRWReRI7oiedVsbVbRvPSmEPnREgsyxGgTpo8pJN+jrcXP27wLBMgA10xcio7orPEASpBD7IrN6TD2essYEjSalC3SAT7UCAwEAAQ==";
    //code that u got from myket
    //String base64EncodedPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCGASqP+iAfWp5HYaEfBCfGjpfnbKvh8qYztgnckAPfZ/MNfCkuM7C2HRMP4vjQBXsSMqQU91E8qwe3yugoQBzx6MCeTA2d3MRhaX/SHrZhyA26ymGa+7MwMC6/JPQZb97cK9XDOB/jTdmOWuOjh32cgiQQhJXy60WPZq4pjDhI4wIDAQAB";
    //code that u got from iranapps
    //String base64EncodedPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC9vEARNaee6FBkYn4lYBVg9W3UukPMJPBV89Fy5/LI2GbSaUX0n6xcLNfkb0tqtP0OGWxSrExO4SIj51GSII8+ldiLjrT1aFctlw/N0hacpAbNZIGach6+Thj3oXM3rGcJoKyIs9l41ZBaprO42IvwfcV74LiHiPzgCzdTyEN12QIDAQAB";
    // Parshub
    //String base64EncodedPublicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDXiynZyMSyogw414A/Ct+fs/hKgZsLpkonfofLowiVc0iabeRuPyxEGT0unuEJeNRtynw7/DfgLkoQKVy9Tnjwv1ScJUil5VQ1iFcGGpVoNmG1ESkzdAdj1A44XMJZ8dSa5Or9U2jXZGojnxpDz1iBwF2kZF94COuLB27LtjUanwIDAQAB";
    // AvalMaket
    //String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAh1lYwpkWqsfNAADMPmXV8CyV6ZMASNzxcaXOZ1fgYBcfgnHE6PMSepN8WpVLogyLL2hR2c4cWE5tSdQd9q9OJ3AuWrn6spoEh3eKtBx2Q8NcXxdOxIdwBPjdvCVKSQy13lRq904QPkYKiJ8Zyhr/8AR/3XC4QBb8Cj6zMCA1E8+2i4VpJ2vWaR50UUs6m4wUnRBGUAx1vAzrRsgolNefLUo4F9DLnDmYhSFeUM2HuxIDW3MbFEvCg0Na6bUG1zx0w83FCjh7HaLRxMxdRFSkstMe6SMLDWpTAolC0qzKCYSo+wFYQCSa5rL9/dRuyjJSnnV2aXf5sLcY+slfzB5yFwIDAQAB";
    // Cando....
    //String base64EncodedPublicKey ="MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCC3hfXAyBj1bnrlNilrtdW4U1qkI8FP27usDKinH9w/XQddtbyn/yY+Qpgi9rZqGEiy8g7jqZr6YZAM3hJCB4V6dvZPwdHmF2AgtbQJQGYbk4lfhfzQl+UGUtsRJtiaPoJZ7ZTYFlqlAz0tRR83w5y0NdkHyqnaJYyOBvI9jgmXwIDAQAB";
    // You can find it in your Bazaar console, in the Dealers section.
    // It is recommended to add more security than just pasting it in your source code;
    mHelper = new IabHelper(this, base64EncodedPublicKey);
    mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {

      @Override
      public void onIabSetupFinished(IabResult result) {
        AppController.logger("Setup finished.");
        if (!result.isSuccess()) {
          // Oh noes, there was a problem.
          AppController.logger("Problem setting up In-app Billing: " + result);
        }
        // Hooray, IAB is fully set up!
        mHelper.queryInventoryAsync(mGotInventoryListener);
      }
    });


  }

  //--------------------------------------FUNCTION ----------------------------------------------
  IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
      AppController.logger("Query inventory finished.");
      if (result.isFailure()) {
        AppController.logger("Failed to query inventory: ");
        dialog.dismiss();
       // Toaster.toastRed("روی گوشی شما احتمالا بازار نصب نیست !! و یا اگر نصب هست باید به اینترنت متصل باشید.", 0);
        return;
      } else {
        AppController.logger("Query inventory was successful.");
        // does the user have the premium upgrade?
        AppController.IS_USER_PREMIUM = inventory.hasPurchase(SKU_PREMIUM);
       // update UI accordingly
      //  AppController.logger(AppController.IS_USER_PREMIUM ? "PREMIUM" : "NOT PREMIUM");
      }
      dialog.dismiss();
      if (AppController.IS_USER_PREMIUM) {
        SharedPreferences.Editor newtask = AppController.preferences.edit();
        newtask.putBoolean(KEY, true);
        newtask.apply();
        Toaster.toastGreen(" تبریک ! شما کاربر ویژه هستید. ", 0,false);
        finish();
      } else {
        Toaster.toastRed("شما کاربر عادی هستید! جهت ارتقا بر روی دکمه ارتقا به کاربر ویژه کلیک نمایید.", 0,false);
      }
    }

  };

  public void onCustomPremiumAppButtonClicked() {
    setWaitScreen(true);

        /* TODO: for security, generate your payload here for verification. See the comments on
         * verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         * an empty string, but on a production app you should carefully generate this. */
    String payload = "ahsjahsdjnsxznxbsjdjlsadjksahd";

    mHelper.launchPurchaseFlow(this, SKU_PREMIUM, RC_REQUEST,
      mPurchaseFinishedListener, payload);
  }


  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);


    // Pass on the activity result to the helper for handling
    if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
      super.onActivityResult(requestCode, resultCode, data);
    } else {
      AppController.logger("onActivityResult handled by IABUtil.");
    }

  }

  boolean verifyDeveloperPayload(Purchase p) {
    String payload = p.getDeveloperPayload();
    return true;
  }

  IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
      if (result.isFailure()) {
        setWaitScreen(false);
        return;
      }
      if (!verifyDeveloperPayload(purchase)) {
        complain("Error purchasing. Authenticity verification failed.");
        setWaitScreen(false);
        return;
      }

      AppController.logger("Purchase Successful.");
      if (purchase.getSku().equals(SKU_PREMIUM)) {
        SharedPreferences.Editor newtask = AppController.preferences.edit();
        newtask.putBoolean(KEY, true);
        newtask.apply();
        Toaster.toastGreen(" تبریک ! شما کاربر ویژه هستید. ", 0,false);
        setWaitScreen(false);
        finish();
      }
    }
  };

  // Enables or disables the "please wait" screen.
  void setWaitScreen(boolean set) {
    findViewById(R.id.waitforbazar).setVisibility(set ? View.VISIBLE : View.GONE);
  }

  void complain(String message) {
    alert("Error: " + message);
  }

  void alert(String message) {
    AlertDialog.Builder bld = new AlertDialog.Builder(this);
    bld.setMessage(message);
    bld.setNeutralButton("OK", null);
    bld.create().show();

  }

  //----------------------------Market Management

  //---------------------------------------Ondestroy------------------
  @Override
  public void onDestroy() {
    super.onDestroy();
    AppController.logger("Destroying helper.");
    if (mHelper != null) mHelper.dispose();
    mHelper = null;
  }
}
