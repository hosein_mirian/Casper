package mirian.english.casper.Activity;

import android.app.Dialog;
import android.graphics.Color;
import android.speech.tts.TextToSpeech;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Scanner;

import mirian.english.casper.Helper.SoundManager;
import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.lists.Content;

public class ActivityType5 extends ActivityCheck implements TextToSpeech.OnInitListener {

  ArrayList<ImageView> Images = new ArrayList<>();
  ArrayList<TextView> Texts = new ArrayList<>();
  Button check_bt;
  TextView question_txt, title;
  ArrayList<String> SpeechText = new ArrayList<>();
  int answer;
  ArrayList<String> answer_string = new ArrayList<>();
  int page_number, stage_id;
  ViewGroup mother_radio;
  RadioGroup radioGroup;
  int j = -1;
  ProgressBar progressBar;
  Dialog result_dialog;
  public View.OnClickListener result_click = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
      result_dialog.dismiss();
      if (answer == Integer.valueOf(AppController.ContentList.get(page_number).getAnswer())) {
        //Toaster.toastGreen("درست گفتی !", 0, true);
//--------// set the status true because user answered right
        Content new_content = new Content(
          AppController.ContentList.get(page_number).getId(),
          AppController.ContentList.get(page_number).getStageId(),
          AppController.ContentList.get(page_number).getQuestion_type(),
          AppController.ContentList.get(page_number).getQuestion(),
          AppController.ContentList.get(page_number).getAnswer(),
          AppController.ContentList.get(page_number).getOption(),
          true // set the status true because user answered right
        );
        AppController.ContentList.set(page_number, new_content);
//-----------------------------------------------
        if ((page_number + 1) < AppController.ContentList.size()) {
          finish();
          ActivitySelector.Select(AppController.ContentList.get(page_number + 1).getQuestion_type(), page_number, stage_id);
        } else {
          //this is the last page... go to Activity Result or check if there is
          finish();
          GotoActivity.gotoActivity(ActivityResult.class, AppController.context, -1, -1, null, null, null);
        }
      } else {
        //Toaster.toastRed("اشتباهه !!", 0, true);
        if ((page_number + 1) < AppController.ContentList.size()) {
          finish();
          ActivitySelector.Select(AppController.ContentList.get(page_number + 1).getQuestion_type(), page_number, stage_id);
        } else {
          //this is the last page... go to Activity Result or check if there is
          finish();
          GotoActivity.gotoActivity(ActivityResult.class, AppController.context, -1, -1, null, null, null);
        }

      }
      for (int i = 0; i < AppController.ContentList.size(); i++) {
        AppController.logger("status " + i + " is " + AppController.ContentList.get(i).getStatus());
      }


    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_type5);
    //-------------------Read Bundle----------------------------Define which category is selected...
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      page_number = extras.getInt("id");
      stage_id = extras.getInt("stage_id");
    }

    progressBar = (ProgressBar) findViewById(R.id.progress);
    progressBar.setMax(AppController.ContentList.size());
    progressBar.setProgress(page_number + 1);


    check_bt = (Button) findViewById(R.id.check_bt);
    check_bt.setTypeface(AppController.font.get(0));

    //------Read data and set question.
    title = (TextView) findViewById(R.id.title_txt);
    title.setTypeface(AppController.font.get(0));

    question_txt = (TextView) findViewById(R.id.question_txt);
    question_txt.setText(AppController.ContentList.get(page_number).getQuestion());
    question_txt.setTypeface(AppController.font.get(0));

    //----Read data and set options
    mother_radio = (ViewGroup) findViewById(R.id.mother_radio);
    radioGroup = new RadioGroup(AppController.context);

    String finalOutput;
    Scanner subLine = new Scanner(AppController.ContentList.get(page_number).getOption()).useDelimiter(",");
    AppController.logger("childcount " + radioGroup.getChildCount());
    while (subLine.hasNext()) {
      j++;
      AppController.logger("J " + j);
      finalOutput = subLine.next();
      final RadioButton radio = new RadioButton(AppController.CurrentActivity);
      radio.setTag(String.valueOf(j + 1));
      radio.setText(finalOutput);
      answer_string.add(finalOutput);
      radio.setTextSize(25);
      radio.setTextColor(Color.GRAY);
      radio.setBackgroundColor(Color.WHITE);
      radio.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          AppController.logger("Radio s:  " + view.getTag());
          answer = Integer.valueOf(view.getTag().toString());
          check_bt.setEnabled(true);
        }
      });
      radioGroup.addView(radio);
      radioGroup.setBackgroundColor(Color.WHITE);
    }
    AppController.logger("childcount after " + radioGroup.getChildCount());


/*
    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup radioGroup, int selected) {
      }
    });
*/
    mother_radio.addView(radioGroup);
    check_bt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        AppController.logger("Answer " + answer);
        //dialog show
        result_dialog = new Dialog(AppController.CurrentActivity);
        result_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        result_dialog.setContentView(R.layout.dialoganswer);
        result_dialog.setCancelable(false);
        result_dialog.show();
        TextView answer_txt = (TextView) result_dialog.findViewById(R.id.answer_txt);
        answer_txt.setTypeface(AppController.font.get(0));
        Button result_ok = (Button) result_dialog.findViewById(R.id.ok_bt);
        answer_txt.setText(answer_string.get(Integer.parseInt(AppController.ContentList.get(page_number).getAnswer()) - 1));
        result_ok.setOnClickListener(result_click);
        result_ok.setTypeface(AppController.font.get(1));

        if (answer == Integer.valueOf(AppController.ContentList.get(page_number).getAnswer())) {
          answer_txt.setTextColor(Color.GREEN);
          SoundManager.playBeep(1);
        } else {
          result_ok.setBackgroundColor(Color.RED);
          answer_txt.setTextColor(Color.RED);
          SoundManager.playBeep(2);
        }
      }
    });

  }

  @Override
  public void onInit(int i) {

  }

}
