package mirian.english.casper.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import com.wang.avi.AVLoadingIndicatorView;

import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.webservice.DataBaseConnection;


public class ActivitySplash extends ActivityCheck {

  AVLoadingIndicatorView progress;
  TextView waiting;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);
    AppController.logger("ActivitySplash");

    //Hosein Mirian WebService---------------------------

    //Initialize Database//------------->>
    AppController.db = new DataBaseConnection(this, "casper", AppController.DIR_DATABASE_ADDRESS, 2); // casper database second edition
    AppController.database = AppController.db.getReadableDatabase();

    //check whether user is premium or not
    AppController.IS_USER_PREMIUM = AppController.preferences.getBoolean(ActivityPurchase.KEY, false);

    //check for comments and get users opinion for market...
    AppController.Check_User_Entry = false;
    int count_entry = AppController.preferences.getInt("number_user_entered", 0);
    count_entry++;
    SharedPreferences.Editor editor = AppController.preferences.edit();
    editor.putInt("number_user_entered", count_entry);
    editor.apply();

//    if ((count_entry == 1 || count_entry % 3 == 0) && (count_entry < 10)) // if its the First and second time that user open the app or every fifth times we show him comment
//    {
//      AppController.Check_User_Entry = true; // should show him the comment for market
//    } else {
//      AppController.Check_User_Entry = false; // should show him the comment for market
//    }
    AppController.Check_User_Entry = (count_entry == 1 || count_entry % 3 == 0) && (count_entry < 10);

//-----------------------------------------------------------------
    GotoActivity.gotoActivity(ActivityMain.class, this, -1,-1, null, null,null);

  }

}
