package mirian.english.casper.Activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.CompositeMultiplePermissionsListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.multi.SnackbarOnAnyDeniedMultiplePermissionsListener;

import java.util.ArrayList;

import mirian.english.casper.DexterPermission.SampleErrorListener;
import mirian.english.casper.DexterPermission.SampleMultiplePermissionListener;
import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;


public class ActivityPermission extends ActivityCheck {
  private MultiplePermissionsListener allPermissionsListener;
  private PermissionRequestErrorListener errorListener;

  ViewGroup rootView;
  Dialog dialogPermission;
  ArrayList<String> PermissionList = new ArrayList<>();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_permission);
//--------Permission Initialize // add as much as Permission you will have.
    PermissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
    PermissionList.add(Manifest.permission.READ_PHONE_STATE);

//    PermissionList.add(Manifest.permission.READ_CONTACTS);
//-------------------
    rootView = (ViewGroup) findViewById(android.R.id.content);


    /// check SDK > 23 --------------->>>
//-----------------
    if (Build.VERSION.SDK_INT >= 23) {

      Boolean check_permission = AppController.preferences.getBoolean("check_permission_"+AppController.App_Name, false);
      if (check_permission) {
        initialize();
      } else {
        createPermissionListeners();
        dialogPermission = new Dialog(ActivityPermission.this);
        dialogPermission.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogPermission.setContentView(R.layout.dialog_permission);
        dialogPermission.setCancelable(false);
        dialogPermission.show();
        TextView title_permission = (TextView) dialogPermission.findViewById(R.id.title_permission);
        Button confirmPermission = (Button) dialogPermission.findViewById(R.id.Permission_bt);
        TextView alert_text = (TextView) dialogPermission.findViewById(R.id.alert_text);
        alert_text.setTypeface(AppController.font.get(0));
        title_permission.setTypeface(AppController.font.get(1));
        confirmPermission.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
            Dexter.withActivity(ActivityPermission.this)
              .withPermissions(PermissionList)
              .withListener(allPermissionsListener)
              .withErrorListener(errorListener)
              .check();
          }
        });


      }

    } else // if its Api is under 23 then....
    {
      initialize();
    }
  }

  //----------------------

  public void showPermissionGranted(String permission) {
    for (int i = 0; i < PermissionList.size(); i++) {
      if (PermissionList.get(i).contentEquals(permission)) {
        PermissionList.remove(i);
      }

    }
    // if all the permission are authorized.
    if (PermissionList.size() == 0) {
      // Toaster.toastGreen("دسترسی های مورد نظر تایید شد. خوش آمدید.", 0);
      initialize();
      dialogPermission.dismiss();
    }
  }

  public void showPermissionDenied(String permission, boolean isPermanentlyDenied) {

    Toaster.toastRed("لطفا مجددا دسترسی های مورد نظر را تایید نمایید.", 0,false);

  }
  //------------------------

  private void createPermissionListeners() {
    MultiplePermissionsListener feedbackViewMultiplePermissionListener =
      new SampleMultiplePermissionListener(this);

    allPermissionsListener =
      new CompositeMultiplePermissionsListener(feedbackViewMultiplePermissionListener,
        SnackbarOnAnyDeniedMultiplePermissionsListener.Builder.with(rootView,
          R.string.all_permissions_denied_feedback)
          .withOpenSettingsButton(R.string.permission_rationale_settings_button_text)
          .build());
    //------------------
    errorListener = new SampleErrorListener();


  }


  @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
  public void showPermissionRationale(final PermissionToken token) {
    new AlertDialog.Builder(this).setTitle(R.string.permission_rationale_title)
      .setMessage(R.string.permission_rationale_message)
      .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          dialog.dismiss();
          token.cancelPermissionRequest();
        }
      })
      .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          dialog.dismiss();
          token.continuePermissionRequest();
        }
      })
      .setOnDismissListener(new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
          token.cancelPermissionRequest();
        }
      })
      .show();
  }

  private void initialize() {
//-- set permission in preferances---->>>
    SharedPreferences.Editor editor = AppController.preferences.edit();
    editor.putBoolean("check_permission_"+AppController.App_Name, true);
    editor.apply();
    //Go to SplashActivity//------------->>
    GotoActivity.gotoActivity(ActivitySplash.class, AppController.context, -1,-1, null, null,null);
  }
}