package mirian.english.casper.Activity;

import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.AnimationAdapter;
import jp.wasabeef.recyclerview.adapters.SlideInBottomAnimationAdapter;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.PieChartView;
import mirian.english.casper.Helper.SoundManager;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.lists.Result;
import mirian.english.casper.lists.ResultAdapter;

public class ActivityResult extends ActivityCheck {
  PieChartView chart;
  PieChartData data;
  float true_count = (float) 0.0;
  int false_count = 0;
  Button check_bt;
  TextView true_answers, false_answers, title;
  RecyclerView recyclerView;
  ResultAdapter adapter;
  List<Result> ResultList;
  Result result;
  int TABLE_SIZE = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_result);

    SoundManager.playBeep(3); // result page sound

    true_answers = (TextView) findViewById(R.id.true_answers);
    false_answers = (TextView) findViewById(R.id.false_answers);
    title = (TextView) findViewById(R.id.title);
    true_answers.setTypeface(AppController.font.get(0));
    false_answers.setTypeface(AppController.font.get(0));
    title.setTypeface(AppController.font.get(3));
    title.setText(AppController.Stage_Name);
    chart = (PieChartView) findViewById(R.id.chart);
    reset();
    generateData();
    check_bt = (Button) findViewById(R.id.check_bt);
    check_bt.setTypeface(AppController.font.get(0));
    check_bt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        finish();
      }
    });
    //-------------------Recycle show the questioner  }
    recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
    RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
    recyclerView.setLayoutManager(mLayoutManager);
    prepareAlbums();
  }

  //---------------------------------------prepare Album Function
  private void prepareAlbums() {
//---------Load info

    ResultList = new ArrayList<>();
    adapter = new ResultAdapter(this, ResultList);

    final AnimationAdapter animationadapter = new SlideInBottomAnimationAdapter(adapter);
    animationadapter.setFirstOnly(true);
    animationadapter.setDuration(500);
    animationadapter.setInterpolator(new OvershootInterpolator(.5f));
    recyclerView.setAdapter(animationadapter);

    TABLE_SIZE = AppController.ContentList.size();
    AppController.logger("Size of All Records : " + TABLE_SIZE); // Get the size of  table
    //recyclerView.addItemDecoration(new ActivityResult.GridSpacingItemDecoration(TABLE_SIZE, dpToPx(-4), true));

//---------------------------------------INITIAL LOAD 10 Item...

//    ArrayList<Struct_Category> data = LoadFromDatabase.load("INITIAL_LOAD", "category", -1); // return max id in the table as Arraylist<Struct>
//    AppController.logger("size data: " + data.size()); // Max Id in the table is ...;
    for (int i = 0; i < TABLE_SIZE; i++) {
      result = new Result(i + 1, AppController.ContentList.get(i).getQuestion_type(), AppController.ContentList.get(i).getQuestion(), AppController.ContentList.get(i).getOption(), AppController.ContentList.get(i).getStatus(), AppController.ContentList.get(i).getAnswer());
      ResultList.add(result);
    }
    //Update Adapter...
    adapter.notifyDataSetChanged();
  }
//----------------------------------End of Prepare()............

  /**
   * RecyclerView item decoration - give equal margin around grid item
   */
  public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

    private int spanCount;
    private int spacing;
    private boolean includeEdge;

    GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
      this.spanCount = spanCount;
      this.spacing = spacing;
      this.includeEdge = includeEdge;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
      int position = parent.getChildAdapterPosition(view); // item position
      int column = position % spanCount; // item column

      if (includeEdge) {
        outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
        outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

        if (position < spanCount) { // top edge
          outRect.top = spacing;
        }
        outRect.bottom = spacing; // item bottom
      } else {
        outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
        outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
        if (position >= spanCount) {
          outRect.top = spacing; // item top
        }
      }
    }
  }

  /**
   * Converting dp to pixel
   */
  private int dpToPx(int dp) {
    Resources r = getResources();
    return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
  }

  //----------------------
  private void reset() {
    chart.setInteractive(false);
    chart.setCircleFillRatio(1.0f);
  }

  private void generateData() {
    for (int i = 0; i < AppController.ContentList.size(); i++) {
      if (AppController.ContentList.get(i).getStatus())
        true_count++;
    }
    true_answers.setText("پاسخ های صحیح : " + (int) true_count);
    false_count = (int) (AppController.ContentList.size() - true_count);
    false_answers.setText("پاسخ های غلط : " + false_count);
    AppController.logger("true_count " + true_count);
    float portion_true = (true_count / AppController.ContentList.size()) * 100;
    AppController.logger("portion_true " + portion_true);
    float portion_false = 100 - portion_true;
    AppController.logger("portion_true : " + portion_true + " portion_false : " + portion_false);
    int numValues = 2;
    List<SliceValue> values = new ArrayList<>();
    SliceValue sliceValue;
    if (portion_true != 100.0) {
      for (int i = 0; i < numValues; i++) {
        if (i == 0)
          sliceValue = new SliceValue(portion_true, ChartUtils.COLOR_GREEN);
        else
          sliceValue = new SliceValue(portion_false, ChartUtils.COLOR_RED);

        values.add(sliceValue);
      }

    } else if (portion_true == 100.0) {
      sliceValue = new SliceValue(portion_true, ChartUtils.COLOR_GREEN);
      values.add(sliceValue);
    } else if (portion_false == 100.0) {
      sliceValue = new SliceValue(portion_false, ChartUtils.COLOR_RED);
      values.add(sliceValue);
    }

    data = new PieChartData(values);

    for (SliceValue value : data.getValues()) {
      Log.i("", " A : " + value.getValue());
      value.setTarget(value.getValue() + 10);
    }
    chart.startDataAnimation();
    chart.setPieChartData(data);
  }
}
