package mirian.english.casper.Activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.Typeface;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

import mirian.english.casper.Helper.FlowLayout;
import mirian.english.casper.Helper.SoundManager;
import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.lists.Content;

public class ActivityType2 extends ActivityCheck implements TextToSpeech.OnInitListener {
  ArrayList<TextView> Texts = new ArrayList<>();
  Button check_bt;
  TextView question_txt, title_txt;
  ArrayList<String> SpeechText = new ArrayList<>();
  ArrayList<String> Selected_text = new ArrayList<>();
  String answer = "";
  int page_number,stage_id;
  FlowLayout answer_grp, option_grp;
  ProgressBar progressBar;
  Dialog result_dialog;
  public View.OnClickListener result_click = new View.OnClickListener() {
    @Override
    public void onClick(View view) {
      result_dialog.dismiss();
      if (answer.contentEquals(AppController.ContentList.get(page_number).getAnswer().replace(" ",""))) {
        //Toaster.toastGreen("درست گفتی !", 0,true);
//--------// set the status true because user answered right
        Content new_content = new Content(
          AppController.ContentList.get(page_number).getId(),
          AppController.ContentList.get(page_number).getStageId(),
          AppController.ContentList.get(page_number).getQuestion_type(),
          AppController.ContentList.get(page_number).getQuestion(),
          AppController.ContentList.get(page_number).getAnswer(),
          AppController.ContentList.get(page_number).getOption(),
          true // set the status true because user answered right
        );
        AppController.ContentList.set(page_number, new_content);
//-----------------------------------------------
        AppController.logger("asa" + page_number + 1 + "size :" + AppController.ContentList.size());
        if ((page_number + 1) < AppController.ContentList.size()) {
          finish();
          ActivitySelector.Select(AppController.ContentList.get(page_number + 1).getQuestion_type(),page_number,stage_id);
        } else {
          //this is the last page... go to Activity Result or check if there is
          finish();
          GotoActivity.gotoActivity(ActivityResult.class,AppController.context,-1,-1,null,null,null);
        }
      } else {
        //Toaster.toastRed("اشتباهه !!", 0,true);
        if ((page_number + 1) < AppController.ContentList.size()) {
          finish();
          ActivitySelector.Select(AppController.ContentList.get(page_number + 1).getQuestion_type(),page_number,stage_id);
        } else {
          //this is the last page... go to Activity Result or check if there is
          finish();
          GotoActivity.gotoActivity(ActivityResult.class,AppController.context,-1,-1,null,null,null);

        }

      }
      for (int i = 0; i < AppController.ContentList.size(); i++) {
        AppController.logger("status " + i + " is " + AppController.ContentList.get(i).getStatus());
      }

    }
  };
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_type2);
    //-------------------Read Bundle----------------------------Define which category is selected...
    Bundle extras = getIntent().getExtras();
    if (extras != null) {
      page_number = extras.getInt("id");
      stage_id = extras.getInt("stage_id");

    }

    progressBar = (ProgressBar) findViewById(R.id.progress);
    progressBar.setMax(AppController.ContentList.size());
    progressBar.setProgress(page_number+1);

    //----- Text to Speach
    if (AppController.tts == null)
      AppController.tts = new TextToSpeech(AppController.context, this);

    check_bt = (Button) findViewById(R.id.check_bt);
    check_bt.setTypeface(AppController.font.get(0));
    //----Read data and set options
    String finalOutput;
    Scanner subLine = new Scanner(AppController.ContentList.get(page_number).getOption()).useDelimiter(",");
    while (subLine.hasNext()) {
      finalOutput = subLine.next();
      SpeechText.add(finalOutput);
    }
    AppController.logger(SpeechText.toString());
    //------Read data and set question.
    question_txt = (TextView) findViewById(R.id.question_txt);
    title_txt = (TextView) findViewById(R.id.title_txt);
    question_txt.setTypeface(AppController.font.get(5));
    title_txt.setTypeface(AppController.font.get(0));
    String speakout = getResources().getString(R.string.speakout);
    question_txt.setText(AppController.ContentList.get(page_number).getQuestion()+" "+speakout);
    question_txt.setOnClickListener(new View.OnClickListener() {
                                  @Override
                                  public void onClick(View view) {
                                    AppController.tts.speak(question_txt.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
                                  }
                                });
  answer_grp = (FlowLayout) findViewById(R.id.answer_grp);
    option_grp = (FlowLayout) findViewById(R.id.option_grp);
    if (option_grp.getChildCount() != 0) {
      Log.i("LOG", " True ");
      option_grp.removeViews(0, AppController.ContentList.size());
    }
    for (int i = 0; i < SpeechText.size(); i++) {
/*
      final TextView textView = new TextView(this);
      textView.setText("  ");
      textView.setTextSize(30);
      textView.setClickable(false);
      textView.setTypeface(Typeface.DEFAULT_BOLD);
      option_grp.addView(textView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
*/
      option_grp.addView(createDummyTextView("  " + SpeechText.get(i) + "  ", i), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
    }
    //---Check_bt
    check_bt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        AppController.logger(":"+AppController.ContentList.get(page_number).getAnswer());
        for (int i = 0; i < Selected_text.size(); i++) {
          answer += Selected_text.get(i);
        }
        AppController.logger(":" + answer);

        //dialog show
        result_dialog = new Dialog(AppController.CurrentActivity);
        result_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        result_dialog.setContentView(R.layout.dialoganswer);
        result_dialog.setCancelable(false);
        result_dialog.show();
        TextView answer_txt = (TextView) result_dialog.findViewById(R.id.answer_txt);
        answer_txt.setTypeface(AppController.font.get(0));
        Button result_ok = (Button) result_dialog.findViewById(R.id.ok_bt);
        answer_txt.setText(AppController.ContentList.get(page_number).getAnswer());
        result_ok.setOnClickListener(result_click);
        result_ok.setTypeface(AppController.font.get(1));

        if (answer.contentEquals(AppController.ContentList.get(page_number).getAnswer().replace(" ",""))) {
          answer_txt.setTextColor(Color.GREEN);
          SoundManager.playBeep(1);
        }else
        {
          result_ok.setBackgroundColor(Color.RED);
          answer_txt.setTextColor(Color.RED);
          SoundManager.playBeep(2);
        }

      }
    });
  }

  //-------------
  private View createDummyTextView(String text, int tag) {
    final TextView textView = new TextView(this);
    textView.setTag(Integer.toString(tag));
    textView.setText(text);
    textView.setBackgroundColor(Color.LTGRAY);
    textView.setTextSize(30);
    textView.setClickable(true);
    textView.setTypeface(AppController.font.get(0));
    textView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(final View view) {
        Log.i("LOG", textView.getText().toString());
        AppController.tts.speak(textView.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
/*
        final TextView textView = new TextView(ActivityType2.this);
        textView.setText("  ");
        textView.setTextSize(30);
        textView.setClickable(false);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        answer_grp.addView(textView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
*/


//        AppController.logger("s: " + option_grp.getChildAt(1).getTag());
        answer_grp.addView(createDummyTextViewDelete("  " + SpeechText.get(Integer.valueOf(view.getTag().toString())) + "  ", Integer.valueOf(view.getTag().toString())), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        Selected_text.add(SpeechText.get(Integer.valueOf(view.getTag().toString())));
        // AppController.logger("sss"+ view.getTag().toString());
        // option_grp.removeViewAt(Integer.valueOf(view.getTag().toString()));
        option_grp.removeView(view);

        AppController.logger("childCount option_grp : " + option_grp.getChildCount());

        if (answer_grp.getChildCount() > 0)
          check_bt.setEnabled(true);
        else
          check_bt.setEnabled(false);

      }
    });
    return textView;
  }

  //-------------
  private View createDummyTextViewDelete(String text, int tag) {
    final TextView textView = new TextView(this);
    textView.setTag(Integer.toString(tag));
    textView.setText(text);
    textView.setBackgroundColor(Color.LTGRAY);
    textView.setTextSize(30);
    textView.setClickable(true);
    textView.setTypeface(AppController.font.get(0));
    textView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Log.i("LOG", textView.getText().toString());
        AppController.tts.speak(textView.getText().toString(), TextToSpeech.QUEUE_FLUSH, null);
/*
        final TextView textView = new TextView(ActivityType2.this);
        textView.setText("  ");
        textView.setTextSize(30);
        textView.setClickable(false);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        option_grp.addView(textView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
*/
        // AppController.logger("s: "+ option_grp.getChildAt(1).getTag());
        option_grp.addView(createDummyTextView("  " + SpeechText.get(Integer.valueOf(view.getTag().toString())) + "  ", Integer.valueOf(view.getTag().toString())), new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        // answer_grp.removeViewAt(Integer.valueOf(view.getTag().toString()));
        Selected_text.remove(SpeechText.get(Integer.valueOf(view.getTag().toString())));
        answer_grp.removeView(view);

        AppController.logger("childCount answer_grp : " + answer_grp.getChildCount());

        if (answer_grp.getChildCount() > 0)
          check_bt.setEnabled(true);
        else
          check_bt.setEnabled(false);

      }
    });
    return textView;
  }

  //-------------
  @Override
  public void onInit(int status) {
    if (status == TextToSpeech.SUCCESS) {
      int result = AppController.tts.setLanguage(Locale.US);
      // tts.setPitch(5); // set pitch level
      // tts.setSpeechRate(2); // set speech speed rate
      if (result == TextToSpeech.LANG_MISSING_DATA
        || result == TextToSpeech.LANG_NOT_SUPPORTED) {
        Log.i("TTS", "Language is not supported");
      } else {
      }
    } else {
      Log.i("TTS", "Initilization Failed");
    }

  }
}
