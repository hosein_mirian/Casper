package mirian.english.casper.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mirian.english.casper.Helper.Fonts;
import mirian.english.casper.Markets_Helper.ManagePermission;
import mirian.english.casper.Notification.BefrestAuthSample;
import mirian.english.casper.lists.Content;
import mirian.english.casper.lists.Struct_Content;
import mirian.english.casper.webservice.DataBaseConnection;
import rest.bef.BefrestFactory;


public class AppController extends Application {

  public static File file;
  public static int iabPermissionId = 0; // every market has its own permission
  private RequestQueue mRequestQueue;
  public static List<Content> ContentList;
  public static Context context;
  public static LayoutInflater inflater;
  private static AppController mInstance;
  public static Activity CurrentActivity;
  public static DataBaseConnection db;
  public static SQLiteDatabase database;
  public static Handler handler = new Handler();
  public static TextToSpeech tts;
  public static String Stage_Name;
  //WebService Local
//  public static String URL_INFO = "http://192.168.1.12/learnenglishbymovie/webservice.php";
//  public static String URL_ROOT = "http://192.168.1.12/learnenglishbymovie/";
//WebService Online
  public static String URL_INFO = "http://www.appbazan.ir/learnenglishbymovie/webservice.php";
  public static String URL_ROOT = "http://www.appbazan.ir/learnenglishbymovie/";


  public static final String TAG = AppController.class.getSimpleName();
  public static final String SD_CARD = Environment.getExternalStorageDirectory().getAbsolutePath();
  public static final String DIR_DATABASE_ADDRESS = SD_CARD + "/casper/";

  public static String LOG_TAG = "TAG";
  public static int DatabaseSize = 0;
  public static ArrayList<Typeface> font = new ArrayList<>();
  public static SharedPreferences preferences;
  public static JSONObject Send_Parameter = new JSONObject(); // in order to send the parameters as JsonObject
  public static boolean IS_USER_PREMIUM = false; // is user premium or not
  public static boolean Check_User_Entry = false;
  public static String App_Name;
  public static MediaPlayer player= new MediaPlayer();

  public static void logger(String msg) {
    Log.i("LOG", msg);
  }

  @Override
  public void onCreate() {
    super.onCreate();

    context = getApplicationContext();
    inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    mInstance = this;
    font = Fonts.Add_Fonts_To_Project();
    preferences = PreferenceManager.getDefaultSharedPreferences(context);
    iabPermissionId = ManagePermission.listIABpermission(); // give the market list if its bazar 1 if its mayket 2 and so on...
    //iabPermissionId = 6; // Only do it for Cando Market
    //logger("permission value is " + iabPermissionId);
    //notify-------------------------------------

    String cellcode = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    AppController.logger("code IMIE : " + cellcode);


    Resources appR = context.getResources();
    App_Name = String.valueOf(appR.getText(appR.getIdentifier("app_name", "string", context.getPackageName())));
    logger(App_Name);


    BefrestAuthSample befrest = new BefrestAuthSample();
    String auth = befrest.generateAuthToken("C7FA24FA7C563CAF347E9ED4EBE6F939", "12345678910111213141516171819202122232425262728-", "/xapi/1/subscribe/10171/"+cellcode+"/2");
    Log.i("LOG", "Auth is :" + auth);
    BefrestFactory.getInstance(this)
      .init(10171, auth, cellcode) // کانال ناتیفی رو بر اساس کدIMIE هرگوشی تعیین کردیم
      .start();


  }


  public static synchronized AppController getInstance() {
    return mInstance;
  }

  public RequestQueue getRequestQueue() {
    if (mRequestQueue == null) {
      mRequestQueue = Volley.newRequestQueue(getApplicationContext());
    }
    return mRequestQueue;
  }


  public <T> void addToRequestQueue(Request<T> req, String tag) {
    // set the default tag if tag is empty
    req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
    getRequestQueue().add(req);
  }

  public <T> void addToRequestQueue(Request<T> req) {
    req.setTag(TAG);
    getRequestQueue().add(req);
  }

  public void cancelPendingRequests(Object tag) {
    if (mRequestQueue != null) {
      mRequestQueue.cancelAll(tag);
    }
  }

}