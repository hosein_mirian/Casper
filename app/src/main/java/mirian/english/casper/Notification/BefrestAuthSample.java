package mirian.english.casper.Notification;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class BefrestAuthSample {

    public String generateAuthToken(String apiKey, String sharedKey, String addr) {
        try {
            String initialPayload = String.format("%s,%s", apiKey, addr);
            byte[] md5 = md5(initialPayload);
            String base64 = base64Encode(md5);

            String payload = String.format("%s,%s", sharedKey, base64);
            md5 = md5(payload);
            return base64Encode(md5);
        } catch (Exception e) {
            // Log the occurred exception
            return null;
        }
    }

    private byte[] md5(String input) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(input.getBytes());
        return messageDigest.digest();
    }

    private String base64Encode(byte[] input) {
        return new String(Base64.encode(input,0))
                .replaceAll("\\+", "-")
                .replaceAll("=", "")
                .replaceAll("/", "_")
                .replaceAll("\n", "");
    }

}
