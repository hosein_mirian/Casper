package mirian.english.casper.Notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mirian.english.casper.Activity.ActivityPermission;
import mirian.english.casper.Markets_Helper.MarketOpenPage;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import rest.bef.BefrestMessage;
import rest.bef.BefrestPushReceiver;

public class PushReceiver extends BefrestPushReceiver {
  NotificationCompat.Builder builder;
  Boolean checkNotifyMode = false;
  ArrayList<StructNotify> json = new ArrayList<>();

  @Override
  public void onPushReceived(Context context, BefrestMessage[] messages) {


    json.clear();
    try {
      JSONObject obj = new JSONObject(messages[0].getData());
      JSONArray itemsArray = obj.getJSONArray("items");

      for (int i = 0; i < itemsArray.length(); i++) {
        JSONObject object = itemsArray.getJSONObject(i);
        StructNotify j = new StructNotify();

        if (object.getString("notify_mode").contentEquals("public")) {
          j.notify_name = object.getString("notify_name");
          j.notify_mode = object.getString("notify_mode");
          j.notify_text = object.getString("notify_text");
          json.add(j);
        } else if (object.getString("notify_mode").contentEquals("new_version_smart"))// agar update version hast
        {
          j.notify_name = object.getString("notify_name");
          j.notify_mode = object.getString("notify_mode");
          j.notify_text = object.getString("notify_text");
          j.notify_market_name = object.getString("notify_market_name");
          json.add(j);
        }else if (object.getString("notify_mode").contentEquals("new_version_casper"))// agar update version hast
        {
          j.notify_name = object.getString("notify_name");
          j.notify_mode = object.getString("notify_mode");
          j.notify_text = object.getString("notify_text");
          j.notify_market_name = object.getString("notify_market_name");
          json.add(j);
        }

      }


    } catch (JSONException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();

    } finally {
      if (!json.isEmpty() && json.get(0).notify_mode.contentEquals("new_version_casper")) {
        if (json.get(0).notify_market_name.contentEquals("hamrahpay"))
          AppController.iabPermissionId = 0;
        if (json.get(0).notify_market_name.contentEquals("bazaar"))
          AppController.iabPermissionId = 1;
        if (json.get(0).notify_market_name.contentEquals("mayket"))
          AppController.iabPermissionId = 2;
        if (json.get(0).notify_market_name.contentEquals("iranapps"))
          AppController.iabPermissionId = 3;
        if (json.get(0).notify_market_name.contentEquals("jhoobin"))
          AppController.iabPermissionId = 4;
        if (json.get(0).notify_market_name.contentEquals("avalmarket"))
          AppController.iabPermissionId = 5;

        if (AppController.iabPermissionId != 0) // if notify has not come from Hamraypay then...
        {
          Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
          Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
          builder = new NotificationCompat.Builder(context)
            .setSmallIcon(R.mipmap.ic_launcher) //replace with your app icon if it is not correct
            .setLargeIcon(largeIcon)
            .setTicker(json.get(0).notify_name)
            .setContentText(json.get(0).notify_text)
            .setSound(alarmSound)
            .setAutoCancel(true);
          MarketOpenPage.openLink();
          NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
          notificationManager.notify(0, builder.build());
        }

      } else if (!json.isEmpty() && json.get(0).notify_mode.contentEquals("new_version_smart")) {

//        PendingIntent contentIntent = PendingIntent.getActivity(AppController.context, 1, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
//        AppController.context.startActivity(intent);

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_smart);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        builder = new NotificationCompat.Builder(context)
          .setSmallIcon(R.mipmap.ic_launcher) //replace with your app icon if it is not correct
          .setLargeIcon(largeIcon)
          .setTicker(json.get(0).notify_name)
          .setContentText(json.get(0).notify_text)
          .setSound(alarmSound)
          .setAutoCancel(true);
        MarketOpenPage.openLinkApp("hosein.learnenglishmovie");
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());

      }
    }




/*
    NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
      .setSmallIcon(R.mipmap.ic_launcher) //replace with your app icon if it is not correct
      .setTicker("پیام از بفرست!")
      .setContentText(messages[0].getData())
      .setAutoCancel(true);

    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    notificationManager.notify(0, builder.build());
*/


  }
}