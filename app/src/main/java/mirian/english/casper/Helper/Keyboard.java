package mirian.english.casper.Helper;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import mirian.english.casper.app.AppController;


public class Keyboard {
    public static void open_keyboard()
    {
        InputMethodManager imm = (InputMethodManager) AppController.context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }
    public static void close_keyboard()
    {
                    InputMethodManager imm = (InputMethodManager) AppController.context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

    }
}
