package mirian.english.casper.Helper;

import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import mirian.english.casper.Markets_Helper.MarketOpenPage;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;

public class RecommendApp {
  public static void show_dialog() {
    final Dialog app_dialog = new Dialog(AppController.CurrentActivity);
    app_dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    app_dialog.setContentView(R.layout.dialogapp);
    app_dialog.show();
    final TextView desc = (TextView) app_dialog.findViewById(R.id.desc_txt);
    final ImageView gotoapp = (ImageView) app_dialog.findViewById(R.id.img_app);
    final TextView desc_app = (TextView) app_dialog.findViewById(R.id.desc_app);
    desc.setTypeface(AppController.font.get(1));
    desc_app.setTypeface(AppController.font.get(0));
    Button yes_bt = (Button) app_dialog.findViewById(R.id.yes_bt);
    Button no_bt = (Button) app_dialog.findViewById(R.id.no_bt);
    yes_bt.setTypeface(AppController.font.get(0));
    no_bt.setTypeface(AppController.font.get(0));
    yes_bt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        app_dialog.dismiss();
        MarketOpenPage.openLinkApp("hosein.learnenglishmovie");
      }
    });
    no_bt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        app_dialog.dismiss();
      }
    });
    gotoapp.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        MarketOpenPage.openLinkApp("hosein.learnenglishmovie");
      }
    });

  }


}
