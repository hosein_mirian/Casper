package mirian.english.casper.Helper;


import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;

import mirian.english.casper.app.AppController;

public class SoundManager {
  public static void playBeep(int status) {
    //status 1 = success 2= failure
    if (AppController.player != null) {
      AppController.player.stop();
      AppController.player.release();
      AppController.player = null;
      AppController.player = new MediaPlayer();
    }

    switch (status) {
      case 1:
        try {

          AssetFileDescriptor descriptor = AppController.context.getAssets().openFd("success.mp3");
          AppController.player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
          descriptor.close();

          AppController.player.prepare();
          AppController.player.setVolume(1f, 1f);
          AppController.player.setLooping(false);
          AppController.player.start();
        } catch (Exception e) {
          e.printStackTrace();
        }

        break;
      case 2:
        try {


          AssetFileDescriptor descriptor = AppController.context.getAssets().openFd("failure.mp3");
          AppController.player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
          descriptor.close();

          AppController.player.prepare();
          AppController.player.setVolume(1f, 1f);
          AppController.player.setLooping(false);
          AppController.player.start();
        } catch (Exception e) {
          e.printStackTrace();
        }

        break;
      case 3:
        try {


          AssetFileDescriptor descriptor = AppController.context.getAssets().openFd("result.mp3");
          AppController.player.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
          descriptor.close();

          AppController.player.prepare();
          AppController.player.setVolume(1f, 1f);
          AppController.player.setLooping(false);
          AppController.player.start();
        } catch (Exception e) {
          e.printStackTrace();
        }

        break;
    }
  }

}
