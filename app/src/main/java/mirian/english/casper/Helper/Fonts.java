package mirian.english.casper.Helper;

import android.graphics.Typeface;

import java.util.ArrayList;

import mirian.english.casper.app.AppController;


public class Fonts {

  public static ArrayList<Typeface> Add_Fonts_To_Project() {
    ArrayList<Typeface> font = new ArrayList<>();
    font.add(Typeface.createFromAsset(AppController.context.getAssets(), "fonts/" + "Syekan.ttf"));
    font.add(Typeface.createFromAsset(AppController.context.getAssets(), "fonts/" + "BNazanin.ttf"));
    font.add(Typeface.createFromAsset(AppController.context.getAssets(), "fonts/" + "BRoyaBd.ttf"));
    font.add(Typeface.createFromAsset(AppController.context.getAssets(), "fonts/" + "mj.ttf"));
    font.add(Typeface.createFromAsset(AppController.context.getAssets(), "fonts/" + "icomoon.ttf"));
    font.add(Typeface.createFromAsset(AppController.context.getAssets(), "fonts/" + "icomoon2.ttf"));

    return font;

  }

}
