package mirian.english.casper.Helper;

import android.graphics.Color;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import mirian.english.casper.app.AppController;


public class Toaster {

  public static void toaster() {
    Toast toast = Toast.makeText(AppController.context,
      "با توجه به فروش آنلاین کالاها ، مسولیت کسری اقلام فاکتورهای دیر ارسال شده به عهده مشتری خواهد بود.", Toast.LENGTH_LONG);
    toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
    toast.getView().setBackgroundColor(Color.RED);
    LinearLayout a = (LinearLayout) toast.getView();
    TextView t = (TextView) a.getChildAt(0);
    //t.setTypeface(AppController.face);
    t.setTextSize(15);
    t.setTextColor(Color.WHITE);
    toast.show();
  }


  public static void NetAlarm() {
    Toaster.toastRed("لطفا به اینترنت متصل شده و سپس امتحان نمایید.", 0,false);
  }


  public static void toast(String St, int font) {
    if (St != null) {
      Toast toast = Toast.makeText(AppController.context,
        St, Toast.LENGTH_LONG);
      toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
      LinearLayout a = (LinearLayout) toast.getView();
      TextView t = (TextView) a.getChildAt(0);
      t.setTextColor(Color.WHITE);
      t.setTextSize(15);
      t.setTypeface(AppController.font.get(font));
      toast.show();
    }
  }


  public static void toastGreen(String St, int font, boolean play_sound) {
    if (St != null) {
      Toast toast = Toast.makeText(AppController.context,
        St, Toast.LENGTH_SHORT);
      toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
      toast.getView().setBackgroundColor(Color.parseColor("#258c05"));
      LinearLayout a = (LinearLayout) toast.getView();
      TextView t = (TextView) a.getChildAt(0);
      t.setTypeface(AppController.font.get(font));
      t.setTextColor(Color.WHITE);
      t.setTextSize(25);
      toast.show();

      if(play_sound)
      SoundManager.playBeep(1);
    }
  }

  public static void toastRed(String St, int font,boolean play_sound) {
    if (St != null) {
      Toast toast = Toast.makeText(AppController.context,
        St, Toast.LENGTH_SHORT);
      toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
      toast.getView().setBackgroundColor(Color.RED);
      LinearLayout a = (LinearLayout) toast.getView();
      TextView t = (TextView) a.getChildAt(0);
      t.setTypeface(AppController.font.get(font));
      t.setTextColor(Color.WHITE);
      t.setTextSize(25);
      toast.show();

      if(play_sound)
        SoundManager.playBeep(2);

    }
  }


  public static void smsAlarm() {
    Toast toast = Toast.makeText(AppController.context,
      "تعداد دفعات تلاش برای ورود به برنامه پایان یافته است. ", Toast.LENGTH_LONG);
    toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
    toast.show();
    // GotoActivity.gotoActivity(ActivityBlock.class, G.context, 0, "", "", null);
  }


  public static void DisabledPart() {
    Toast toast = Toast.makeText(AppController.context,
      "این بخش فعال نمی باشد. ", Toast.LENGTH_LONG);
    toast.setGravity(Gravity.CENTER | Gravity.CENTER_HORIZONTAL, 0, 0);
    toast.show();
  }

}
