package mirian.english.casper.Helper;

import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;


public class TextViewAdder {

    //--->> Add TextViews in array And Set Font Automaticly----<<
    private ArrayList<TextView> Text_view_Array = new ArrayList<TextView>();
    private String              yekanfont       = "Syekan.ttf";
  //  private Typeface            defaultFont     = Typeface.createFromAsset(A.context.getAssets(), "font/" + yekanfont + ""); // khandane fonte morede nazar;


    public TextViewAdder()
    {

        Text_view_Array.clear();

    }


    public void ResetArray()
    {
        Text_view_Array.clear();
    }


    public void AddItem(View textview, Typeface font)
    {
        if (font != null)
        {
            Text_view_Array.add((TextView) textview);
            Text_view_Array.get(Text_view_Array.size() - 1).setTypeface(font);
        } else
        {
            Text_view_Array.add((TextView) textview);
           // Text_view_Array.get(Text_view_Array.size() - 1).setTypeface(defaultFont);
        }

    }

}
