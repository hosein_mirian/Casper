package mirian.english.casper.Helper;

import android.content.Context;
import android.net.ConnectivityManager;

import mirian.english.casper.app.AppController;


public class IsOnline {

    public static boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) AppController.context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
