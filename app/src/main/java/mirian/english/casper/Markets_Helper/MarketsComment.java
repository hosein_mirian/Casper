package mirian.english.casper.Markets_Helper;

import android.content.Intent;
import android.net.Uri;

import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.app.AppController;


public class MarketsComment {

  public static void comment() {
    Boolean Check_Market_Is_Installed;
    Check_Market_Is_Installed = MarketIsInstalled.isPackageInstalled(MarketsName.getMarketPackage());

    if (!Check_Market_Is_Installed) {// if its not installed then ...
      Toaster.toastRed("برای کامنت گذاری برای برنامه ما لطفا برنامه " + MarketsName.getMarketName() + " را نصب نمایید. ", 0,false);
      return;
    }
    switch (AppController.iabPermissionId) {
      case 1:
        comment("com.farsitel.bazaar", "bazaar://details?id=", "https://cafebazaar.ir/app/");
        break;
      case 2:
        comment("ir.mservices.market", "myket://comment?id=", "https://myket.ir/app/");
        break;
      case 3:
        comment("ir.tgbs.android.iranapp", "iranapps://app/", "http://iranapps.ir/app/");
        break;
      case 4:
        comment("net.jhoobin.jhub", "parshub://search?q=", "http://www.parshub.com/push/GAME/930503884&"); //jhoobin
        break;
      case 5:
        comment("avalmarket", "?", "?"); //avalmarket
        break;
      default:
        Toaster.toastRed("سیستم نظر دهی برای این مارکت وجود ندارد !", 0,false);
        break;
    }
  }

  //---------------------------------------
  private static void comment(String pack, String marketAPI, String marketURL) {
    try {
      Intent intent = new Intent(Intent.ACTION_EDIT);
      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      intent.setPackage(pack);
      intent.setData(Uri.parse(marketAPI + AppController.context.getPackageName()));
      AppController.context.startActivity(intent);
    } catch (Exception e) {
      Intent intent = new Intent(Intent.ACTION_VIEW);
      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      intent.setPackage(null);
      intent.setData(Uri.parse(marketURL + AppController.context.getPackageName()));
      AppController.context.startActivity(intent);
    }
  }
  //--------------------------
/*
    مایکت
    اول مارکت
    کافه بازار
    ایران اپس
    مارکت های پارس هاب و چارخونه (مشترک تحت نظر ژوبین)
  <uses-permission android:name="ir.mservices.market.BILLING" />
  <uses-permission android:name="com.hrm.android.market.permission.PAY_THROUGH_MARKET" />
  <uses-permission android:name="com.farsitel.bazaar.permission.PAY_THROUGH_BAZAAR" />
  <uses-permission android:name="ir.tgbs.iranapps.permission.BILLING"/>
  <uses-permission android:name="net.jhoobin.InAppBilling" />
*/
}