package mirian.english.casper.Markets_Helper;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import mirian.english.casper.app.AppController;


public class ManagePermission {
  public static int listIABpermission() {
    try {
      PackageManager packageManager = AppController.context.getPackageManager();
      PackageInfo packageInfo = packageManager.getPackageInfo(AppController.context.getPackageName(), PackageManager.GET_PERMISSIONS);
      String[] permissions = packageInfo.requestedPermissions;

      for (String per: permissions) {
        if (per.equals("com.farsitel.bazaar.permission.PAY_THROUGH_BAZAAR")) {
          return 1;
        } else if (per.equals("ir.mservices.market.BILLING")) {
          return 2;
        } else if (per.equals("ir.tgbs.iranapps.permission.BILLING")) {
          return 3;
        } else if (per.equals("net.jhoobin.InAppBilling")) {
          return 4;
        } else if (per.equals("com.hrm.android.market.permission.PAY_THROUGH_MARKET")) {
          return 5;
        }
      }
      return 0;
    }
    catch (Exception e) {
      e.printStackTrace();
      return 0;
    }
  }

}
