package mirian.english.casper.Markets_Helper;


import mirian.english.casper.app.AppController;

public class MarketsName {
  public static String getMarketName() {
    String return_string = "";
    switch (AppController.iabPermissionId) {
      case 1:
        return_string = "بازار";
        break;
      case 2:
        return_string = "مایکت";
        break;
      case 3:
        return_string = "ایران اپس";
        break;
      case 4:
        return_string = "جوبین";
        break;
      case 5:
        return_string = "اول مارکت";
        break;
      case 6:
        return_string = "کندو";
        break;

      default:
        return_string = "بازار";
        break;
    }

    return return_string;
  }

  //------------------------------
  public static String getMarketPackage() {
    String return_package_string = "";
    switch (AppController.iabPermissionId) {
      case 1:
        return_package_string = "com.farsitel.bazaar";
        break;
      case 2:
        return_package_string = "ir.mservices.market";
        break;
      case 3:
        return_package_string = "ir.tgbs.android.iranapp";
        break;
      case 4:
        return_package_string = "net.jhoobin.jhub";
        break;
      case 5:
        return_package_string = "avalmarket";
        break;
      case 6:
        return_package_string = "com.ada.market";
        break;
      default:
        return_package_string = "com.farsitel.bazaar";
        break;
    }

    return return_package_string;
  }
}
