package mirian.english.casper.Markets_Helper;

import android.content.pm.PackageManager;

import mirian.english.casper.app.AppController;


public class MarketIsInstalled {
  public  static  boolean isPackageInstalled(String PackageName) {
    //com.farsitel.bazaar
    PackageManager manager = AppController.context.getPackageManager();
    boolean isAppInstalled = false;
    try {
      manager.getPackageInfo(PackageName, PackageManager.GET_ACTIVITIES);
      isAppInstalled = true;
    } catch (PackageManager.NameNotFoundException e) {
      e.printStackTrace();
    }
    return isAppInstalled;
  }

}
