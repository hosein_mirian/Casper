package mirian.english.casper.Markets_Helper;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.app.AppController;


public class MarketOpenPage {
  public static void openLink() {
    Boolean Check_Market_Is_Installed;
    Check_Market_Is_Installed = MarketIsInstalled.isPackageInstalled(MarketsName.getMarketPackage());

    if (!Check_Market_Is_Installed) {// if its not installed then ...
      Toaster.toastRed("برای دیدن این برنامه باید " + MarketsName.getMarketName() + " را نصب نمایید. ", 0,false);
      return;
    }

    String appUrl = "";
    String pakage = "";
    switch (AppController.iabPermissionId) {
      case 1:
        appUrl = "https://cafebazaar.ir/app/";
        pakage = "com.farsitel.bazaar";
        break;
      case 2:
        appUrl = "myket://comment?id=";
        pakage = "ir.mservices.market";
        break;
      case 3:
        appUrl = "ir.tgbs.android.iranapp";
        pakage = "iranapps://app/";
        break;
      case 4:
        appUrl = "jhoobin://search?q=";
        pakage = null; //"net.jhoobin.jhub";
        break;
      case 5:
        appUrl = "avalmarket";
        pakage = "?";
        break;
      case 6:
        appUrl = "http://cando.asr24.com/app.jsp?package=";
        pakage = "com.ada.market";
        break;
      default:
        appUrl = "https://cafebazaar.ir/app/";
        pakage = "com.farsitel.bazaar";
        break;
    }
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(appUrl + AppController.context.getPackageName()));
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.setPackage(pakage);
    try {
      AppController.context.startActivity(intent);
    }
    catch (ActivityNotFoundException ex) {
      intent.setPackage(null);
      AppController.context.startActivity(intent);
    }

  }
  public static void openLinkApp(String App_Address)
  {
    Boolean Check_Market_Is_Installed;
    Check_Market_Is_Installed = MarketIsInstalled.isPackageInstalled(MarketsName.getMarketPackage());

    if (!Check_Market_Is_Installed) {// if its not installed then ...
      Toaster.toastRed("برای دیدن سایر برنامه ها باید " + MarketsName.getMarketName() + " را نصب نمایید. ", 0,false);
      return;
    }

    String appUrl = "";
    String pakage = "";
    switch (AppController.iabPermissionId) {
      case 1:
        appUrl = "https://cafebazaar.ir/app/";
        pakage = "com.farsitel.bazaar";
        break;
      case 2:
        appUrl = "http://myket.ir/app/";
        pakage = "ir.mservices.market";
        break;
      case 3:
        appUrl = "http://iranapps.ir/app/";
        pakage = "ir.tgbs.iranapps";
        break;
      case 4:
        appUrl = "jhoobin://search?q=";
        pakage = null; //"net.jhoobin.jhub";
        break;
      case 5:
        appUrl = "avalmarket";
        pakage = "?";
        break;
      case 6:
        appUrl = "cando://details?id=";
        pakage = "com.ada.market";
        break;
      default:
        appUrl = "https://cafebazaar.ir/app/";
        pakage = "com.farsitel.bazaar";
        break;
    }
    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(appUrl + App_Address));
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    intent.setPackage(pakage);
    try {
      AppController.context.startActivity(intent);
    }
    catch (ActivityNotFoundException ex) {
      intent.setPackage(null);
      AppController.context.startActivity(intent);
    }
  }
}
