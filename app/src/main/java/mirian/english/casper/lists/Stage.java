package mirian.english.casper.lists;


public class Stage {
  private int id, cat_id, stage_id;
  private String stage_name, desc;

  public Stage() {
  }

  public Stage(int id, int cat_id, int stage_id, String stage_name, String desc) {
    this.id = id;
    this.cat_id = cat_id;
    this.stage_id = stage_id;
    this.stage_name = stage_name;
    this.desc = desc;
  }

  public String getStage_name() {
    return stage_name;
  }

  public void setStage_name(String stage_name) {
    this.stage_name = stage_name;
  }

  public String getDesc() {
    return desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public int getCat_id() {
    return cat_id;
  }

  public void setCat_id(int cat_id) {
    this.cat_id = cat_id;
  }

  public int get_id() {
    return id;
  }

  public void set_id(int id) {
    this.id = id;
  }

  public int getStage_id() {
    return stage_id;
  }

  public void setStage_id(int stage_id) {
    this.stage_id = stage_id;
  }


}
