package mirian.english.casper.lists;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mirian.english.casper.Activity.ActivityPurchase;
import mirian.english.casper.Activity.ActivityStage;
import mirian.english.casper.Activity.GotoActivity;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

  private Context mContext;
  private List<Category> categoryList;
  CategoryAdapter adapter;


  public class MyViewHolder extends RecyclerView.ViewHolder {
    TextView cat_name, course_passed;
    ImageView pic, lock;
    FrameLayout frame_click;
    CardView cardView;


    public MyViewHolder(View view) {
      super(view);
      cat_name = (TextView) view.findViewById(R.id.cat_name_txt);
      pic = (ImageView) view.findViewById(R.id.pic);
      lock = (ImageView) view.findViewById(R.id.lock_img);
      cardView = (CardView) view.findViewById(R.id.card_view);
      frame_click = (FrameLayout) view.findViewById(R.id.frame_click);
    }
  }


  public CategoryAdapter(Context mContext, List<Category> albumList) {
    this.mContext = mContext;
    this.categoryList = albumList;
    this.adapter = this;
  }

  @Override
  public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
      .inflate(R.layout.category_card, parent, false);
    return new MyViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(final MyViewHolder holder, int position) {
    final Category category = categoryList.get(position);
    holder.cat_name.setText(category.getCat_Name());
    holder.cat_name.setTypeface(AppController.font.get(0));
    holder.cardView.setCardBackgroundColor(Color.parseColor(category.getColor()));
    holder.lock.setVisibility(View.GONE);
    int resourceID = AppController.context.getResources().getIdentifier(category.getPic(), "mipmap", AppController.context.getPackageName());
    holder.pic.setImageResource(resourceID);

    if (category.getCat_id() != 1 && category.getCat_id() != 2 ) {
      if(!AppController.IS_USER_PREMIUM)
      {
      holder.cardView.setCardBackgroundColor(Color.parseColor("#8c8c8c"));
      holder.lock.setVisibility(View.VISIBLE);
      }
    }
    holder.frame_click.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (category.getCat_id() != 1 && category.getCat_id() != 2) { // if the user is not premium then go to ActivityPurchase
          if(!AppController.IS_USER_PREMIUM)
          {
          GotoActivity.gotoActivity(ActivityPurchase.class, AppController.context, -1, -1, null, null, null);
          }else
          {
            GotoActivity.gotoActivity(ActivityStage.class, AppController.context, category.getCat_id(), -1, category.getCat_Name(), category.getPic(), category.getColor());
          }
        } else {
          GotoActivity.gotoActivity(ActivityStage.class, AppController.context, category.getCat_id(), -1, category.getCat_Name(), category.getPic(), category.getColor());
        }
      }
    });
  }


  /**
   * Showing popup menu when tapping on 3 dots
   */

  /**
   * Click listener for popup menu items
   */

  @Override
  public int getItemCount() {
    return categoryList.size();
  }
}
