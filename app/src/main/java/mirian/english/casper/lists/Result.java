package mirian.english.casper.lists;


public class Result {
  private int id, question_type;
  private boolean status;
  private String question, answer, option, true_answer;

  public Result() {

  }

  public Result(int id, int question_type, String question, String option, boolean status, String true_answer) {
    this.id = id;
    this.question_type = question_type;
    this.question = question;
    this.option = option;
    this.status = status;
    this.true_answer = true_answer;
  }

  public boolean getStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getQuestion_type() {
    return question_type;
  }

  public void setQuestion_type(int question_type) {
    this.question_type = question_type;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public String getTrue_answer() {
    return true_answer;
  }
  public void setTrue_answer(String true_answer) {
    this.true_answer = true_answer;
  }

  public String getOption() {
    return option;
  }

  public void setOption(String option) {
    this.option = option;
  }

}
