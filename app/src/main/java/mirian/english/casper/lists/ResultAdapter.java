package mirian.english.casper.lists;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;
import java.util.Scanner;

import mirian.english.casper.R;
import mirian.english.casper.app.AppController;


public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.MyViewHolder> {

  private Context mContext;
  private List<Result> resultList;
  ResultAdapter adapter;


  public class MyViewHolder extends RecyclerView.ViewHolder {
    TextView question_txt, question_number_txt, your_answer_txt;
    ViewGroup text_container;
    FrameLayout frame_click;
    CardView cardView;


    public MyViewHolder(View view) {
      super(view);

      question_number_txt = (TextView) view.findViewById(R.id.question_number_txt);
      question_txt = (TextView) view.findViewById(R.id.question_txt);
      your_answer_txt = (TextView) view.findViewById(R.id.your_answer_txt);
      text_container = (ViewGroup) view.findViewById(R.id.text_container);
      cardView = (CardView) view.findViewById(R.id.card_view);
      frame_click = (FrameLayout) view.findViewById(R.id.frame_click);
    }
  }


  public ResultAdapter(Context mContext, List<Result> resultList) {
    this.mContext = mContext;
    this.resultList = resultList;
    this.adapter = this;
  }

  @Override
  public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
      .inflate(R.layout.result_card, parent, false);
    return new MyViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(final MyViewHolder holder, int position) {
    final Result result = resultList.get(position);
    holder.question_number_txt.setText("" + result.getId());
    holder.question_txt.setText(result.getQuestion());
    holder.question_txt.setTypeface(AppController.font.get(0));
    holder.question_number_txt.setTypeface(AppController.font.get(0));
    if (result.getStatus()) {
      holder.your_answer_txt.setText(" پاسخ شما " + AppController.context.getResources().getString(R.string.yes));
      holder.your_answer_txt.setTypeface(AppController.font.get(4));
      holder.your_answer_txt.setTextColor(Color.GREEN);
    } else {
      holder.your_answer_txt.setText(" پاسخ شما " + AppController.context.getResources().getString(R.string.no));
      holder.your_answer_txt.setTypeface(AppController.font.get(4));
      holder.your_answer_txt.setTextColor(Color.RED);
    }

    holder.text_container.removeAllViews();

    switch (result.getQuestion_type()) {
      case 1:
        String finalOutput = null;
        Scanner subLine = new Scanner(result.getOption()).useDelimiter(",");
        int j = -1;
        while (subLine.hasNext()) {
          j++;
          finalOutput = subLine.next();
          if (result.getTrue_answer().contentEquals(String.valueOf(j + 1))) {
            TextView ans1 = new TextView(AppController.context);
            ans1.setText(finalOutput);
            ans1.setTextColor(Color.WHITE);
            ans1.setTextSize(21);
            ans1.setTypeface(AppController.font.get(0));
            holder.text_container.addView(ans1);
          }
        }
        break;
      case 2:
        String finalOutput2 = null;
        Scanner subLine2 = new Scanner(result.getOption()).useDelimiter(",");
        int j2 = -1;
        while (subLine2.hasNext()) {
          j2++;
          finalOutput2 = subLine2.next();
          if (result.getTrue_answer().contentEquals(finalOutput2)) {
            TextView ans2 = new TextView(AppController.context);
            ans2.setText(finalOutput2);
            ans2.setTextColor(Color.GREEN);
            ans2.setTextSize(21);
            ans2.setTypeface(AppController.font.get(0));
            holder.text_container.addView(ans2);
          } else {
            TextView ans2 = new TextView(AppController.context);
            ans2.setText(finalOutput2);
            ans2.setTextColor(Color.WHITE);
            ans2.setTextSize(21);
            ans2.setTypeface(AppController.font.get(0));
            holder.text_container.addView(ans2);
          }
          TextView ans2 = new TextView(AppController.context);
          ans2.setText("  ");
          ans2.setTextColor(Color.WHITE);
          ans2.setTextSize(21);
          ans2.setTypeface(AppController.font.get(0));
          holder.text_container.addView(ans2);

        }
        break;
      case 3:
        TextView ans3 = new TextView(AppController.context);
        ans3.setGravity(Gravity.CENTER);
        ans3.setText(result.getTrue_answer());
        ans3.setTextColor(Color.WHITE);
        ans3.setTextSize(21);
        ans3.setTypeface(AppController.font.get(0));
        holder.text_container.addView(ans3);
        break;
      case 4:
        TextView ans4 = new TextView(AppController.context);
        ans4.setText(result.getTrue_answer());
        ans4.setTextColor(Color.WHITE);
        ans4.setTextSize(21);
        ans4.setTypeface(AppController.font.get(0));
        holder.text_container.addView(ans4);
        break;
      case 5:
        String finalOutput5 = null;
        Scanner subLine5 = new Scanner(result.getOption()).useDelimiter(",");
        int jj = -1;
        while (subLine5.hasNext()) {
          jj++;
          finalOutput5 = subLine5.next();
          if (result.getTrue_answer().contentEquals(String.valueOf(jj + 1))) {
            TextView ans5 = new TextView(AppController.context);
            ans5.setText(finalOutput5);
            ans5.setTextColor(Color.WHITE);
            ans5.setTextSize(21);
            ans5.setTypeface(AppController.font.get(0));
            holder.text_container.addView(ans5);
          }
        }

        break;
    }

//    int resourceID = AppController.context.getResources().getIdentifier(category.getPic(), "drawable", AppController.context.getPackageName());
//    holder.pic.setImageResource(resourceID);


    holder.frame_click.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
      }
    });
  }


  /**
   * Showing popup menu when tapping on 3 dots
   */

  /**
   * Click listener for popup menu items
   */

  @Override
  public int getItemCount() {
    return resultList.size();
  }
}
