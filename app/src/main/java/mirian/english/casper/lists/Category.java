package mirian.english.casper.lists;


public class Category {
  private int cat_id;
  private String cat_name, pic, color;

  public Category() {
  }

  public Category(int cat_id, String cat_name, String pic, String color) {
    this.cat_id = cat_id;
    this.cat_name = cat_name;
    this.pic = pic;
    this.color = color;
  }

  public String getCat_Name() {
    return cat_name;
  }

  public void setCat_name(String cat_name) {
    this.cat_name = cat_name;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic;
  }

  public int getCat_id() {
    return cat_id;
  }

  public void setCat_id(int cat_id) {
    this.cat_id = cat_id;
  }


}
