package mirian.english.casper.lists;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mirian.english.casper.Activity.ActivityStage;
import mirian.english.casper.Activity.GotoActivity;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;


public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.MyViewHolder> {

  private Context mContext;
  private List<Content> contentList;
  ContentAdapter adapter;


  public class MyViewHolder extends RecyclerView.ViewHolder {
    TextView cat_name, course_passed;
    ImageView pic;
    FrameLayout frame_click;
    CardView cardView;


    public MyViewHolder(View view) {
      super(view);
      cat_name = (TextView) view.findViewById(R.id.cat_name_txt);
      pic = (ImageView) view.findViewById(R.id.pic);
      cardView = (CardView) view.findViewById(R.id.card_view);
      frame_click = (FrameLayout) view.findViewById(R.id.frame_click);
    }
  }


  public ContentAdapter(Context mContext, List<Content> albumList) {
    this.mContext = mContext;
    this.contentList = albumList;
    this.adapter = this;
  }

  @Override
  public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
      .inflate(R.layout.category_card, parent, false);
    return new MyViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(final MyViewHolder holder, int position) {
    final Content content = contentList.get(position);
//    holder.cat_name.setText(category.getCat_Name());
//    holder.cat_name.setTypeface(AppController.font.get(0));
//    holder.cardView.setCardBackgroundColor(Color.parseColor(category.getColor()));

//    int resourceID = AppController.context.getResources().getIdentifier(category.getPic(), "drawable", AppController.context.getPackageName());
//    holder.pic.setImageResource(resourceID);


    holder.frame_click.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        //GotoActivity.gotoActivity(ActivityStage.class, AppController.context, category.getCat_id(), category.getCat_Name(), category.getPic(),category.getColor());
      }
    });
  }


  /**
   * Showing popup menu when tapping on 3 dots
   */

  /**
   * Click listener for popup menu items
   */

  @Override
  public int getItemCount() {
    return contentList.size();
  }
}
