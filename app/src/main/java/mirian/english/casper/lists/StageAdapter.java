package mirian.english.casper.lists;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mirian.english.casper.Activity.ActivitySelector;
import mirian.english.casper.R;
import mirian.english.casper.app.AppController;
import mirian.english.casper.webservice.LoadFromDatabase;


public class StageAdapter extends RecyclerView.Adapter<StageAdapter.MyViewHolder> {

  private Context mContext;
  private List<Stage> stageList;
  StageAdapter adapter;


  public class MyViewHolder extends RecyclerView.ViewHolder {
    TextView stage_name, course_passed,desc_txt;
    ImageView pic_img;
    CardView cardView;
    Button enter_bt;

    public MyViewHolder(View view) {
      super(view);
      stage_name = (TextView) view.findViewById(R.id.stage_name_txt);
      desc_txt = (TextView) view.findViewById(R.id.desc_txt);
      cardView = (CardView) view.findViewById(R.id.card_view);
      enter_bt = (Button) view.findViewById(R.id.enter_bt);
    }
  }


  public StageAdapter(Context mContext, List<Stage> albumList) {
    this.mContext = mContext;
    this.stageList = albumList;
    this.adapter = this;
  }

  @Override
  public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(parent.getContext())
    .inflate(R.layout.stage_card, parent, false);
    return new MyViewHolder(itemView);
  }

  @Override
  public void onBindViewHolder(final MyViewHolder holder, int position) {
    final Stage stage = stageList.get(position);

    holder.stage_name.setText(stage.getStage_name());
    holder.desc_txt.setText(stage.getDesc());
    holder.stage_name.setTypeface(AppController.font.get(0));
    holder.enter_bt.setTypeface(AppController.font.get(0));
    holder.enter_bt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        try {
          Content content;
          AppController.ContentList = new ArrayList<>();
          AppController.ContentList.clear();
          ArrayList<Struct_Content> data = LoadFromDatabase.load("INITIAL_LOAD", "content", stage.getStage_id()); // return max id in the table as Arraylist<Struct>
          AppController.logger("size data: " + data.size()); // Max Id in the table is ...;
          for (int i = 0; i < data.size(); i++) {
            content = new Content(data.get(i).id, data.get(i).stage_id, data.get(i).question_type, data.get(i).question, data.get(i).answer, data.get(i).option,false);
            AppController.ContentList.add(content);
          }
          //----------------------------------------------
          AppController.logger(" "+AppController.ContentList.get(0).getQuestion_type()+" "+AppController.ContentList.get(0).getQuestion());
          AppController.logger("Size of ContentList "+AppController.ContentList.size());

          //----------------------------------------------
          AppController.Stage_Name = stage.getStage_name();
          ActivitySelector.Select(AppController.ContentList.get(0).getQuestion_type(),-1,AppController.ContentList.get(0).getStageId());

        }catch (Exception e)
        {
          e.printStackTrace();
        }
      }
    });
  }


  /**
   * Showing popup menu when tapping on 3 dots
   */

  /**
   * Click listener for popup menu items
   */

  @Override
  public int getItemCount() {
    return stageList.size();
  }
}
