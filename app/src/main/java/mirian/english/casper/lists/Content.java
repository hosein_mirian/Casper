package mirian.english.casper.lists;


public class Content {
  private int id, stage_id, question_type;
  private boolean status;
  private String question, answer, option;

  public Content() {
  }

  public Content(int id, int stage_id, int question_type, String question, String answer, String option,boolean status) {
    this.id = id;
    this.stage_id = stage_id;
    this.question_type = question_type;
    this.question = question;
    this.answer = answer;
    this.option = option;
    this.status = status;
  }

  public boolean getStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getStageId() {
    return stage_id;
  }

  public void setStage_id(int stage_id) {
    this.stage_id = stage_id;
  }

  public int getQuestion_type() {
    return question_type;
  }

  public void setQuestion_type(int question_type) {
    this.question_type = question_type;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }
  public String getOption() {
    return option;
  }

  public void setOption(String option) {
    this.option = option;
  }

}
