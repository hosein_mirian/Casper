package mirian.english.casper.webservice;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mirian.english.casper.Activity.ActivityManager;
import mirian.english.casper.Helper.IsOnline;
import mirian.english.casper.Helper.Toaster;
import mirian.english.casper.app.AppController;


public class webService {
  public static void GetProduct(Context context, final AVLoadingIndicatorView progress, final String url, final String save_type, JSONObject Send_Parameter) {
    //-----------Variables
    final String _URL;
    final JSONObject _Send_Parameter;
    final Boolean _progressStatus;
    //-------------Initialize
    _Send_Parameter = Send_Parameter;
    if (progress == null) {
      _progressStatus = false; // default it is False;
    } else {
      _progressStatus = true;
    }
    if (url == null) {
      Toaster.toastRed("URL is NULL", 0,false);
      return;
    } else {
      _URL = url;
    }


//--- -------------Execute------------------
    final StringRequest SendReq = new StringRequest(Request.Method.POST,
      _URL, new Response.Listener<String>() {

      @Override
      public void onResponse(final String response) {


        new Thread(new Runnable() {
          @Override
          public void run() {
            try {
              AppController.logger("response is: "+response);

              if(!response.contentEquals("null")) {
                SaveToDatabase.Save(save_type, response);
              }
              else
              {
                AppController.logger("null");
                if (_progressStatus) {
                 progress.hide();
                }
               ActivityManager.FinishSplashActivity("Activity.ActivitySplash"); // Check if it is in splashActivity then go to maninActivty
              }
              //SaveToDatabase.Save(null, response);
              // if just save_type is null -> No data will save. Just send to Server is important.


              AppController.handler.post(new Runnable() {

                @Override
                public void run() {
                  if (save_type != null) // if it is a save type request, then...
                  {
                    Toaster.toastGreen("اطلاعات به درستی دریافت و ذخیره گردید", 0,false);
                    ActivityManager.FinishSplashActivity("Activity.ActivitySplash"); // Check if it is in splashActivity then go to maninActivty
                  } else// else it is just a send type request without saving , then...
                  {
                    Toaster.toastGreen("اطلاعات به درستی ارسال گردید", 0,false);
                    ActivityManager.FinishSplashActivity("Activity.ActivitySplash"); // Check if it is in splashActivity then go to maninActivty
                  }
                  if (_progressStatus) {
                    progress.hide();
                  }
                }
              });
            } catch (JSONException e) {
              e.printStackTrace();
             ActivityManager.FinishSplashActivity("Activity.ActivitySplash"); // Check if it is in splashActivity then go to maninActivty

            }

          }

        }).start();


      }


    }, new Response.ErrorListener() {

      @Override
      public void onErrorResponse(VolleyError error) {
        if (save_type != null)// if it is a save type request, then...
        {
          Toaster.toastRed("دریافت و دخیره اطلاعات انجام نشد!! لطفا مجددا تلاش نمایید.", 0,false);
          ActivityManager.FinishSplashActivity("Activity.ActivitySplash"); // Check if it is in splashActivity then go to maninActivty
        } else// else it is just a send type request without saving , then...
        {
          Toaster.toastRed("ارسال اطلاعات ناموفقیت آمیز بود.لطفا مجددا تلاش نمایید.", 0,false);
          ActivityManager.FinishSplashActivity("Activity.ActivitySplash"); // Check if it is in splashActivity then go to maninActivty
        }
        if (_progressStatus) {
          progress.hide();
        }
      }
    }) {

      @Override
      protected Map<String, String> getParams() throws AuthFailureError {
        // Posting parameters to login url
        Map<String, String> params = new HashMap<>();
        if (_Send_Parameter != null)
          params.put("json", _Send_Parameter.toString());
        return params;
      }

    };

    //-------------------------------------------------------------------------------------
    if (IsOnline.isOnline()) {
      SendReq.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
      AppController.getInstance().addToRequestQueue(SendReq);

      if (_progressStatus) {
        AppController.handler.post(new Runnable() {
                                     @Override
                                     public void run() {
                                       progress.show();
                                     }
                                   });
      }
    } else {
      Toaster.NetAlarm();
    }

  }


}


//-----------
