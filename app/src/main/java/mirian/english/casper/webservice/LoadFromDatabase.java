package mirian.english.casper.webservice;


import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import mirian.english.casper.app.AppController;
import mirian.english.casper.lists.Struct_Category;
import mirian.english.casper.lists.Struct_Content;
import mirian.english.casper.lists.Struct_Product;
import mirian.english.casper.lists.Struct_Stage;
import mirian.english.casper.lists.Struct_Translate;


public class LoadFromDatabase {
  public static <any> any load(String command, String table, int cat_id) { // any means we the function accept any kind of data..
    any temp = null;
    ArrayList<any> Final_Return_Struct = new ArrayList<any>();


    AppController.database = AppController.db.getReadableDatabase();


    //----------------MAX---------------------------
    if (command.contentEquals("LOAD_ALL")) {
      Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table, null);
      while (cursor.moveToNext()) {
        try {
          if (table.contentEquals("catagory")) {
            AppController.logger("id: " + cursor.getInt(cursor.getColumnIndex("id")) + " _cat_id: " + cursor.getInt(cursor.getColumnIndex("cat_id"))
              + " _cat_name: " + cursor.getString(cursor.getColumnIndex("cat_name"))
              + " _pic: " + cursor.getString(cursor.getColumnIndex("pic"))
              + " _big_pic: " + cursor.getString(cursor.getColumnIndex("big_pic"))
            );
          } else if (table.contentEquals("product")) {
            AppController.logger("id: " + cursor.getInt(cursor.getColumnIndex("id"))
              + " _cat_id: " + cursor.getInt(cursor.getColumnIndex("cat_id"))
              + " _name : " + cursor.getString(cursor.getColumnIndex("name"))
              + " _date : " + cursor.getString(cursor.getColumnIndex("date"))
              + " _video : " + cursor.getString(cursor.getColumnIndex("video"))
              + " _cat_pic :" + cursor.getString(cursor.getColumnIndex("pic"))
              + " _isnew :" + cursor.getString(cursor.getColumnIndex("isnew"))
              + " subtitle :" + cursor.getString(cursor.getColumnIndex("subtitle"))
              + " video_downloaded :" + cursor.getString(cursor.getColumnIndex("video_downloaded"))
              + " media_type :" + cursor.getString(cursor.getColumnIndex("media_type"))
              + " video_size :" + cursor.getString(cursor.getColumnIndex("video_size"))
              + " is_fave :" + cursor.getString(cursor.getColumnIndex("is_fave"))
            );
          }

        } catch (Exception e) {
          Log.i("LOG", "Catch");
        }

      }
      cursor.close();
    }

    //----------------MAX---------------------------
    if (command.contentEquals("MAX")) {
      Cursor cursor = AppController.database.rawQuery("SELECT id FROM " + table + " WHERE id = (SELECT MAX(id)  FROM " + table + " ) ", null);
      while (cursor.moveToNext()) {
        if (table.contentEquals("catagory")) {

          Struct_Category struct = new Struct_Category();
          struct.id = cursor.getInt(cursor.getColumnIndex("id"));
          Final_Return_Struct.add((any) struct);

        } else if (table.contentEquals("product")) {
          Struct_Product struct = new Struct_Product();
          struct.id = cursor.getInt(cursor.getColumnIndex("id"));
          Final_Return_Struct.add((any) struct);

        }
      }
      cursor.close();
    }
    //----------------MIN---------------------------
    else if (command.contentEquals("MIN")) {
      Cursor cursor = AppController.database.rawQuery("SELECT id FROM" + table + " WHERE id = (SELECT MIN(id)  FROM " + table + " ) ", null);
      while (cursor.moveToNext()) {
        if (table.contentEquals("catagory")) {
          Struct_Category struct = new Struct_Category();
          struct.id = cursor.getInt(cursor.getColumnIndex("id"));
          Final_Return_Struct.add((any) struct);
        } else if (table.contentEquals("product")) {
          Struct_Product struct = new Struct_Product();
          struct.id = cursor.getInt(cursor.getColumnIndex("id"));
          Final_Return_Struct.add((any) struct);

        }
      }
      cursor.close();
    }
    //----------------INITIAL_LOAD COMMAND---------------------------
    if (command.contentEquals("INITIAL_LOAD")) {
      if (table.contentEquals("category")) {
        Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table , null);
        while (cursor.moveToNext()) {
          Struct_Category struct = new Struct_Category();
          struct.cat_id = cursor.getInt(cursor.getColumnIndex("cat_id"));
          struct.cat_name = cursor.getString(cursor.getColumnIndex("cat_name"));
          struct.pic = cursor.getString(cursor.getColumnIndex("pic"));
          struct.color = cursor.getString(cursor.getColumnIndex("color"));
          Final_Return_Struct.add((any) struct);
        }
        cursor.close();
      }

      if (table.contentEquals("stages")) {
        Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE cat_id = " + cat_id, null);
        while (cursor.moveToNext()) {
          Struct_Stage struct = new Struct_Stage();
          struct.id = cursor.getInt(cursor.getColumnIndex("id"));
          struct.cat_id = cursor.getInt(cursor.getColumnIndex("cat_id"));
          struct.stage_id = cursor.getInt(cursor.getColumnIndex("stage_id"));
          struct.stage_name = cursor.getString(cursor.getColumnIndex("stage_name"));
          struct.desc = cursor.getString(cursor.getColumnIndex("desc"));
          Final_Return_Struct.add((any) struct);
        }
        cursor.close();

      }

      if (table.contentEquals("content")) {
        Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE stage_id = " + cat_id, null);
        while (cursor.moveToNext()) {
          Struct_Content struct = new Struct_Content();
          struct.id = cursor.getInt(cursor.getColumnIndex("id"));
          struct.stage_id = cursor.getInt(cursor.getColumnIndex("stage_id"));
          struct.question_type = cursor.getInt(cursor.getColumnIndex("question_type"));
          struct.question = cursor.getString(cursor.getColumnIndex("question"));
          struct.answer = cursor.getString(cursor.getColumnIndex("answer"));
          struct.option = cursor.getString(cursor.getColumnIndex("option"));
          Final_Return_Struct.add((any) struct);
        }
        cursor.close();

      }

      if (table.contentEquals("product")) {
        //
        Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE cat_id = " + cat_id + " ORDER BY id DESC LIMIT 10", null);
        while (cursor.moveToNext()) {
          Struct_Product struct = new Struct_Product();
          struct.id = cursor.getInt(cursor.getColumnIndex("id"));
          struct.cat_id = cursor.getInt(cursor.getColumnIndex("cat_id"));
          struct.isnew = cursor.getInt(cursor.getColumnIndex("isnew"));
          struct.video_downloaded = cursor.getInt(cursor.getColumnIndex("video_downloaded"));
          struct.name = cursor.getString(cursor.getColumnIndex("name"));
          struct.pic = cursor.getString(cursor.getColumnIndex("pic"));
          struct.date = cursor.getString(cursor.getColumnIndex("date"));
          struct.video = cursor.getString(cursor.getColumnIndex("video"));
          struct.subtitle = cursor.getString(cursor.getColumnIndex("subtitle"));
          struct.video_size = cursor.getString(cursor.getColumnIndex("video_size"));
          struct.media_type = cursor.getString(cursor.getColumnIndex("media_type"));
          struct.is_fave = cursor.getInt(cursor.getColumnIndex("is_fave"));
          Final_Return_Struct.add((any) struct);
        }
        cursor.close();

      }
    }
    //-----------------------Return and close database
    AppController.database.close();
    return ((any) (Final_Return_Struct));
  }

  //---------------SEARCH FUNCTION---------------------------------------
  public static <any> any Search(String table, String search_text) {
    AppController.database = AppController.db.getReadableDatabase();
    ArrayList<any> Final_Return_Struct = new ArrayList<any>();

    //---------------------SEARCH----------------------------------------

    Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE (name like'" + '%' + search_text + '%' + "')", null);
    while (cursor.moveToNext()) {
      Struct_Product struct = new Struct_Product();
      struct.id = cursor.getInt(cursor.getColumnIndex("id"));
      struct.cat_id = cursor.getInt(cursor.getColumnIndex("cat_id"));
      struct.isnew = cursor.getInt(cursor.getColumnIndex("isnew"));
      struct.video_downloaded = cursor.getInt(cursor.getColumnIndex("video_downloaded"));
      struct.name = cursor.getString(cursor.getColumnIndex("name"));
      struct.pic = cursor.getString(cursor.getColumnIndex("pic"));
      struct.date = cursor.getString(cursor.getColumnIndex("date"));
      struct.video = cursor.getString(cursor.getColumnIndex("video"));
      struct.subtitle = cursor.getString(cursor.getColumnIndex("subtitle"));
      struct.video_size = cursor.getString(cursor.getColumnIndex("video_size"));
      struct.media_type = cursor.getString(cursor.getColumnIndex("media_type"));
      struct.is_fave = cursor.getInt(cursor.getColumnIndex("is_fave"));
      Final_Return_Struct.add((any) struct);

    }
    cursor.close();
    //--------------------------
    AppController.database.close();
    return ((any) (Final_Return_Struct));
  }

  //---------------------SEARCH_PERSIAN_WORD----------------------------------------
  public static <any> any Search_Persian_Word(String table, String search_text) {
    AppController.database = AppController.db.getReadableDatabase();
    ArrayList<any> Final_Return_Struct = new ArrayList<any>();
    AppController.logger(":"+search_text);

    Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE en='" +  search_text + "'", null);
    //Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE field1='" +  search_word + "'", null);
    while (cursor.moveToNext()) {
      Struct_Translate struct = new Struct_Translate();
      struct.fa = cursor.getString(cursor.getColumnIndex("fa"));
      Final_Return_Struct.add((any) struct);
    }
    cursor.close();

    //--------------------------
    AppController.database.close();
    return ((any) (Final_Return_Struct));
  }
  //---------------------SEARCH_ENGLISH_WORD----------------------------------------
  public static <any> any Search_English_Word(String table, String search_text) {
    AppController.database = AppController.db.getReadableDatabase();
    ArrayList<any> Final_Return_Struct = new ArrayList<any>();
    AppController.logger(":"+search_text);

    Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE field1='" +  search_text + "'", null);
    while (cursor.moveToNext()) {
      Struct_Translate struct = new Struct_Translate();
      struct.en = cursor.getString(cursor.getColumnIndex("field2"));
      Final_Return_Struct.add((any) struct);
    }
    cursor.close();

    //--------------------------
    AppController.database.close();
    return ((any) (Final_Return_Struct));
  }


  //---------------FAVORITE FUNCTION---------------------------------------
  public static <any> any Favorite(String table) {
    AppController.database = AppController.db.getReadableDatabase();
    ArrayList<any> Final_Return_Struct = new ArrayList<any>();

    //---------------------FAVORITE----------------------------------------

    Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE is_fave = 1 ORDER BY id DESC LIMIT 10", null);
    while (cursor.moveToNext()) {
      Struct_Product struct = new Struct_Product();
      struct.id = cursor.getInt(cursor.getColumnIndex("id"));
      struct.cat_id = cursor.getInt(cursor.getColumnIndex("cat_id"));
      struct.isnew = cursor.getInt(cursor.getColumnIndex("isnew"));
      struct.video_downloaded = cursor.getInt(cursor.getColumnIndex("video_downloaded"));
      struct.name = cursor.getString(cursor.getColumnIndex("name"));
      struct.pic = cursor.getString(cursor.getColumnIndex("pic"));
      struct.date = cursor.getString(cursor.getColumnIndex("date"));
      struct.video = cursor.getString(cursor.getColumnIndex("video"));
      struct.subtitle = cursor.getString(cursor.getColumnIndex("subtitle"));
      struct.video_size = cursor.getString(cursor.getColumnIndex("video_size"));
      struct.media_type = cursor.getString(cursor.getColumnIndex("media_type"));
      struct.is_fave = cursor.getInt(cursor.getColumnIndex("is_fave"));
      Final_Return_Struct.add((any) struct);

    }
    cursor.close();
    //--------------------------
    AppController.database.close();
    return ((any) (Final_Return_Struct));
  }

  //---------------------------------------------------------------------
  //----------------SIZE_WITH_CONDITION COMMAND------------------------------
  public static int get_size(String table, int cat_id) {
    AppController.database = AppController.db.getReadableDatabase();
    int record_count = 0;
    if (cat_id == -2) //its a favorite size query
    {
      Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE is_fave = 1 ", null);
      record_count = cursor.getCount();
      cursor.close();
      AppController.database.close();
      return record_count;
    }

    if (cat_id == -1) //its a simple query
    {
      Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table, null);
      record_count = cursor.getCount();
      cursor.close();
    } else // it is a query with condition
    {
      Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE cat_id=" + cat_id, null);
      record_count = cursor.getCount();
      cursor.close();
    }
    AppController.database.close();
    return record_count;

    //--------------
  }

  ///LAZY LOAD FUNCTION
  public static <any> any lazy_load(String table, int cat_id, int Offset) {
    ArrayList<any> Final_Return_Struct = new ArrayList<any>();

    AppController.database = AppController.db.getReadableDatabase();
    //----------------SIZE COMMAND------------------------------
    if (table.contentEquals("catagory")) {
      Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + "ORDER BY id DESC LIMIT 10 OFFSET " + Offset, null);
      while (cursor.moveToNext()) {
        Struct_Category struct = new Struct_Category();
        struct.cat_id = cursor.getInt(cursor.getColumnIndex("cat_id"));
        struct.cat_name = cursor.getString(cursor.getColumnIndex("cat_name"));
        struct.pic = cursor.getString(cursor.getColumnIndex("pic"));
//        struct.big_pic = cursor.getString(cursor.getColumnIndex("big_pic"));
        Final_Return_Struct.add((any) struct);
      }
      cursor.close();
    }
    if (table.contentEquals("product") && cat_id != -1) { //
      //
      Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE cat_id = " + cat_id + " ORDER BY id DESC LIMIT 10 OFFSET " + Offset, null);
      while (cursor.moveToNext()) {
        Struct_Product struct = new Struct_Product();
        struct.id = cursor.getInt(cursor.getColumnIndex("id"));
        struct.cat_id = cursor.getInt(cursor.getColumnIndex("cat_id"));
        struct.isnew = cursor.getInt(cursor.getColumnIndex("isnew"));
        struct.video_downloaded = cursor.getInt(cursor.getColumnIndex("video_downloaded"));
        struct.name = cursor.getString(cursor.getColumnIndex("name"));
        struct.pic = cursor.getString(cursor.getColumnIndex("pic"));
        struct.date = cursor.getString(cursor.getColumnIndex("date"));
        struct.video = cursor.getString(cursor.getColumnIndex("video"));
        struct.subtitle = cursor.getString(cursor.getColumnIndex("subtitle"));
        struct.video_size = cursor.getString(cursor.getColumnIndex("video_size"));
        struct.media_type = cursor.getString(cursor.getColumnIndex("media_type"));
        struct.is_fave = cursor.getInt(cursor.getColumnIndex("is_fave"));
        Final_Return_Struct.add((any) struct);
      }
      cursor.close();
    }
    if (table.contentEquals("product") && cat_id == -2) { // it is a favorite query for ActivityFavorite
      //
      Cursor cursor = AppController.database.rawQuery("SELECT * FROM " + table + " WHERE is_fave = 1 ORDER BY id DESC LIMIT 10 OFFSET " + Offset, null);
      while (cursor.moveToNext()) {
        Struct_Product struct = new Struct_Product();
        struct.id = cursor.getInt(cursor.getColumnIndex("id"));
        struct.cat_id = cursor.getInt(cursor.getColumnIndex("cat_id"));
        struct.isnew = cursor.getInt(cursor.getColumnIndex("isnew"));
        struct.video_downloaded = cursor.getInt(cursor.getColumnIndex("video_downloaded"));
        struct.name = cursor.getString(cursor.getColumnIndex("name"));
        struct.pic = cursor.getString(cursor.getColumnIndex("pic"));
        struct.date = cursor.getString(cursor.getColumnIndex("date"));
        struct.video = cursor.getString(cursor.getColumnIndex("video"));
        struct.subtitle = cursor.getString(cursor.getColumnIndex("subtitle"));
        struct.video_size = cursor.getString(cursor.getColumnIndex("video_size"));
        struct.media_type = cursor.getString(cursor.getColumnIndex("media_type"));
        struct.is_fave = cursor.getInt(cursor.getColumnIndex("is_fave"));
        Final_Return_Struct.add((any) struct);
      }
      cursor.close();
    }
    //-----------------------Return and close database
    AppController.database.close();
    return ((any) (Final_Return_Struct));

  }
//-----------------------------LOAD PRODUCTS-------------------------------------


}
