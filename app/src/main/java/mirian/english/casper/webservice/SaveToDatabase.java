package mirian.english.casper.webservice;

import android.content.ContentValues;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mirian.english.casper.app.AppController;


public class SaveToDatabase {
  public  static void Update_Record(int id,String table,String field,int value)
  {
    ContentValues cv = new ContentValues();
    AppController.database = AppController.db.getWritableDatabase();
    cv.put(field, value);
    AppController.database.update(table,cv, "id="+id,null);
    AppController.database.close();

//------Read Database Records....................................
  LoadFromDatabase.load("LOAD_ALL", "product",-1);

}
  static void Save(String save_type, String response) throws JSONException {
    //initialize
    // IF save_type is Null then it is just a send and does not need to save anything.
    if (save_type == null) {
      return;
    }



    //---------------------->>SAVE_CATEGORY
    if (save_type.contentEquals("save_initialize")) {
      AppController.logger("save_initialize");
      JSONArray category, product = null;

      try {
        ///////////////------------TRY--------------------------
        JSONObject mainObject = new JSONObject(response);
        AppController.logger("Size " + mainObject.length());

        if (mainObject.has("data_category")) {
          //AppController.logger("data_category elements are: "+ mainObject.get("data_category"));
          category = mainObject.getJSONArray("data_category");
          //------DELETE AND Write new Records...........into Database....................................
          AppController.database = AppController.db.getWritableDatabase();
          AppController.database.delete("catagory", null, null);

          for (int i = 0; i < category.length(); i++) {
            JSONObject object = category.getJSONObject(i);
            ContentValues cv = new ContentValues();
            cv.put("cat_name", object.getString("cat_name"));
            cv.put("cat_id", object.getInt("cat_id"));//and so on...
            cv.put("pic", object.getString("pic"));
            cv.put("big_pic", object.getString("big_pic"));
            AppController.database.insert("catagory", null, cv);

//        String strSQL = "INSERT INTO test_tb (id,name,pic) VALUES (null," + "'" + object.getString("name") + "','" + object.getString("pic") + "')";
//        AppController.database.execSQL(strSQL);
          }

//------Read Database Records....................................
          LoadFromDatabase.load("LOAD_ALL", "catagory",-1);

        }
        if (mainObject.has("data_product")) {
          AppController.logger("data_product elements are: " + mainObject.get("data_product"));
          product = mainObject.getJSONArray("data_product");
          //---- SAVE_PRODUCT----------------------------------
          AppController.database = AppController.db.getWritableDatabase();
          for (int i = 0; i < product.length(); i++) {
            JSONObject object = product.getJSONObject(i);
            ContentValues cv = new ContentValues();
            cv.put("name", object.getString("name"));
            cv.put("cat_id", object.getInt("cat_id"));//and so on...
            cv.put("date", object.getString("date"));
            cv.put("pic", object.getString("pic"));
            cv.put("video", object.getString("video")+"r"); // adding r to encript the video to not visible in users device as video
            cv.put("subtitle", object.getString("subtitle"));
            cv.put("video_size",object.getString("video_size")+"مگا بایت");
            cv.put("media_type",object.getString("media_type"));
            cv.put("isnew", 1); // it is new, therefore must become 1 in this field.
            cv.put("video_downloaded",0); // has video downloaded or not
            cv.put("is_fave",0); // is favorite or not
            AppController.database.insert("product", null, cv);
          }
//------Read Database Records....................................
          LoadFromDatabase.load("LOAD_ALL", "product",-1);
        }

      }// -------------CATCH-------------------------
      catch (Exception e) {
        e.printStackTrace();
      }
    }

//---------------------->>SAVE_CATEGORY
    /*
    if (save_type.contentEquals("save_category")) {
      AppController.logger("SAVE_CATEGORY");


      JSONObject mainObject = new JSONObject(response);
      JSONArray itemsArray = mainObject.getJSONArray("data");


//------DELETE AND Write new Records...........into Database....................................
      AppController.database = AppController.db.getWritableDatabase();
      AppController.database.delete("catagory",null,null);

      for (int i = 0; i < itemsArray.length(); i++) {
        JSONObject object = itemsArray.getJSONObject(i);
        ContentValues cv = new ContentValues();
        cv.put("cat_name", object.getString("cat_name"));
        cv.put("cat_id", object.getInt("cat_id"));//and so on...
        cv.put("pic", object.getString("pic"));

        AppController.database.insert("catagory", null, cv);

//        String strSQL = "INSERT INTO test_tb (id,name,pic) VALUES (null," + "'" + object.getString("name") + "','" + object.getString("pic") + "')";
//        AppController.database.execSQL(strSQL);
      }

//------Read Database Records....................................
      AppController.database = AppController.db.getReadableDatabase();
      Cursor cursor = AppController.database.rawQuery("SELECT * FROM catagory", null);
      while (cursor.moveToNext()) {
        try {
          AppController.logger(" _cat_id: " + cursor.getInt(cursor.getColumnIndex("cat_id"))
            + " _cat_name: " + cursor.getString(cursor.getColumnIndex("cat_name"))
            + " _pic: " + cursor.getString(cursor.getColumnIndex("pic"))
       );
        } catch (Exception e) {
          Log.i("LOG", "Catch");
        }

      }
      cursor.close();
    }
//---------------------->>SAVE_NEW_PRODUCT TYPE----------------------------------------------------<>
    if (save_type.contentEquals("save_new_product")) {
      AppController.logger("SAVE_NEW_PRODUCT");


      JSONObject mainObject = new JSONObject(response);
      JSONArray itemsArray = mainObject.getJSONArray("data");
      AppController.logger(itemsArray.toString());


//------Write New Records...........into Database....................................

      AppController.database = AppController.db.getWritableDatabase();
      for (int i = 0; i < itemsArray.length(); i++) {
        JSONObject object = itemsArray.getJSONObject(i);
        ContentValues cv = new ContentValues();
        cv.put("name", object.getString("name"));
        cv.put("cat_id", object.getInt("cat_id"));//and so on...
        cv.put("date", object.getString("date"));
        cv.put("pic", object.getString("pic"));
        cv.put("video", object.getString("video"));

        AppController.database.insert("product", null, cv);

//        String strSQL = "INSERT INTO test_tb (id,name,pic) VALUES (null," + "'" + object.getString("name") + "','" + object.getString("pic") + "')";
//        AppController.database.execSQL(strSQL);
      }

//------Read Database Records....................................
      AppController.database = AppController.db.getReadableDatabase();
      Cursor cursor = AppController.database.rawQuery("SELECT * FROM product", null);
      while (cursor.moveToNext()) {
        try {
          AppController.logger(" _cat_id: " + cursor.getInt(cursor.getColumnIndex("cat_id"))
            + " _name: " + cursor.getString(cursor.getColumnIndex("name"))
            + " _date: " + cursor.getString(cursor.getColumnIndex("date"))
            + " _video: " + cursor.getString(cursor.getColumnIndex("video"))
            + " _cat_pic:" + cursor.getString(cursor.getColumnIndex("pic")));
        } catch (Exception e) {
          Log.i("LOG", "Catch");
        }

      }
      cursor.close();
    }
    */
//---------------------->>.....................................SEARCH TYPE
    if (save_type.contentEquals("search")) {
      AppController.logger("SEARCH");
    }
//---------------------->>......................................MY CUSTOM TYPE

//<<<<<<<<<<<<----------------------END---COMMANDS...................................---->>>>>>>>>>>

    //Close Database//.......................
    if (AppController.database.isOpen())
      AppController.database.close();
    //------Read Database Records....................................


  }


}
